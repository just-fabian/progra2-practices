package tarea04;

/**
 * Exercise class.
 * Instruction: Given an array of integers, sum consecutive even numbers and consecutive odd numbers.
 * Repeat the process while it can be done and return the length of the final array.
 */
public class Solution {
    /**
     * This method will sum the consecutive odd or even numbers in the array
     * @param arr the array
     * @param i the current index item to find
     */
    private static void sumConsecutiveOddOrEven(int [] arr, int i){
        int previousIndex = 1;
        int nextIndex = 1;
        int currentSum = arr[i];
        while (i + nextIndex < arr.length - 1 && arr[i] % 2 == arr[i+nextIndex] % 2){
            currentSum += arr[i + nextIndex];
            arr[i + nextIndex] = 0;
            nextIndex++;
        }
        while (
            i - previousIndex >= 0 && (arr[i-previousIndex] == 0 || arr[i] % 2 == arr[i-previousIndex] % 2)
        ){
            currentSum += arr[i-previousIndex];
            arr[i -previousIndex] = 0;
            previousIndex++;
        }
        arr[i] = 0;
        arr[i-previousIndex+1] = currentSum;
    }

    /**
     * This method will return the length of the array with the consecutive odd or even numbers summed
     * @param arr the array to look in
     * @return the length of the final array
     */
    public static int sumGroups(int[] arr) {
        for (int i = 0; i < arr.length; i++){
            sumConsecutiveOddOrEven(arr, i);
        }
        int finalArrayLength = 0;

        for (int j : arr) {
            finalArrayLength = j != 0 ? finalArrayLength + 1 : finalArrayLength;
        }
        return finalArrayLength;
    }
}
