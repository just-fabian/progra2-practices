package tarea07;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JUArrayList<Integer> implements List<Integer>{
    private Integer [] array;
    private int count;

    public JUArrayList(){
        this.array = (Integer[]) new Object[10];
        count = 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<>() {
            int contador = 0;
            public boolean hasNext() {
                return size() > contador;
            }
            public Integer next() {
                Integer item = array[contador];
                contador++;
                return item;
            }
        };
    }

    @Override
    public boolean add(Integer integer) {
        try{
            add(size(), integer);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        try {
            for (int i = 0; i < array.length; i++){
                if (array[i].equals(o)) {
                    remove(i);
                    break;
                }
            }
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public void clear() {
        array = (Integer[]) new Object[10];
        count = 0;
    }

    @Override
    public Integer get(int index) {
        if (index < 0 || index >= size()) throw new IndexOutOfBoundsException("Not a valid index");
        return array[index];
    }

    @Override
    public void add(int index, Integer element) {
        if (index < 0 || index > size()) throw new IndexOutOfBoundsException("Not valid index");
        else{
            if (count == array.length) {
                Integer[] array2 = (Integer[]) new Object[array.length + 10];
                for (int i = 0; i < array.length; i++) {
                    array2[i] = array[i];
                }
                array = array2;
            }

            Integer[] tmpArray = (Integer[]) new Object[array.length + 1];
            for (int i = 0; i < array.length; i++) {
                tmpArray[i] = array[i];
            }

            array[index] = element;
            for (int i = index + 1; i < array.length - 1; i++){
                array[i] = tmpArray[i - 1];
            }
            count++;
        }
    }

    @Override
    public Integer remove(int index) {
        Integer removedElement = array[index];
        for (int j = index; j < count; j++) {
            array[j] = array[j + 1];
        }
        count--;
        return removedElement;
    }

    @Override
    public boolean contains(Object o) {
        boolean foundItem = false;
        for (int i = 0; i < array.length; i++){
            if (o.equals(array[i])){
                foundItem = true;
                break;
            }
        }
        return foundItem;
    }

    @Override
    public Integer set(int index, Integer element) {
        if (index < 0 || index >= size()) throw new IndexOutOfBoundsException("Not valid index");
        Integer previousItem = array[index];
        array[index] = element;
        return previousItem;
    }

    @Override
    public Object[] toArray() {
        Integer[] newArray = (Integer[]) new Object[count];
        for (int i = 0; i < array.length; i++){
            if (array[i] == null) break;
            else newArray[i] = array[i];
        }
        return newArray;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1;
        for (int i = 0; i < array.length; i++){
            if (o.equals(array[i])){
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = -1;
        for (int i = array.length - 1; i >= 0; i--){
            if (o.equals(array[i])){
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public String toString(){
        String arrayToString = "";
        for (int i = 0; i < size(); i++){
            arrayToString += array[i] + ", ";
        }
        return arrayToString;
    }

//  NOT IMPLEMENTED --------------------------------------
    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }


    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }


    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }


    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }
}