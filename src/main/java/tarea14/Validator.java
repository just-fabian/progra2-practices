package tarea14;

import tarea13.Stack;
import tarea14.exceptions.*;
import tarea14.operations.Operation;

public class Validator {
    private Stack<Character> tokens;
    private Stack<Operation> operations;

    private Stack<Integer> operands = new Stack<>();
    private Stack<Character> operators = new Stack<>();

    public Validator(Stack<Operation> operations){
        tokens = new Stack<>();
        this.operations = operations;
    }

    private Operation getCurrentOperator(char operator){
        for (int i = 0; i < operations.size(); i++)
            if (operations.get(i).getOperator() == operator) return operations.get(i);
        return null;
    }

    private boolean isDigit(char token) {
        return Character.isDigit(token);
    }

    private boolean isOperator(char token) {
        return getCurrentOperator(token) != null;
    }

    private boolean isParentheses(char token){
        return token == '(' || token == ')';
    }

    private boolean validateTokens(){
        char token = ' ';
        for (int i = 0; i < tokens.size(); i++){
            token = tokens.get(i);
            if (!isDigit(token) && !isOperator(token) && !isParentheses(token) && token != ' ')
                throw new InvalidTokenException(token);
        }
        return true;
    }

    private void setTokens(String str) {
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
    }

    private void invalidOperationException(String expression){
        throw new InvalidOperationException(expression);
    }

    private void manageParentheses(char temp, String expression){
        if(temp == '(') operators.push(temp);
        else {
            boolean invalid = true;
            while (!operators.isEmpty()) {
                char c = operators.pop();
                if (c == '(') {
                    invalid = false;
                    break;
                }
                else {
                    if (operands.size() < 2 && c != '!') invalidOperationException(expression);
                    else if (c != '!') operands.pop();
                }
            }
            if (invalid) throw new InvalidOperationException(expression);
        }
    }

    private void operatorsVal(String expression){
        boolean hasFactorial = false;
        while (!operators.isEmpty()) {
            char c = operators.pop();
            if (!isOperator(c)) {
                invalidOperationException(expression);
            }
            if (c == '!') hasFactorial = true;
            if (operands.size() < 2 && (!hasFactorial)) {
                invalidOperationException(expression);
            }
            else operands.pop();
        }
        if (operands.size() > 1 || !operators.isEmpty())
            invalidOperationException(expression);
    }

    private int getCurrentNumber(int current, char temp){
        try {
            current = Integer.parseInt(current + "" + temp);
        } catch (Exception e) {
            throw new InvalidOperandException();
        }

        return current;
    }

    private void isOperationValid(String expression) {
        boolean isTrue = true;
        int current = 0;
        for (int i = 0; i < expression.length(); i++) {
            char temp = expression.charAt(i);
            if (temp == ' ') continue;
            if (isDigit(temp)) {
                current = getCurrentNumber(current, temp);
                if(!(i < expression.length() - 1) || !isDigit(expression.toCharArray()[i + 1])) {
                    operands.push(current);
                    current = 0;
                    isTrue = true;
                }
                if (current != 0) isTrue = true;
                if(isTrue)  isTrue = false;
                else throw new InvalidOperationException(expression);
            }
            else if (isOperator(temp)) {
                operators.push(temp);
                isTrue = true;
            }
            else manageParentheses(temp, expression);
        }
        operatorsVal(expression);
    }

    public void validateOperation(String input){
        setTokens(input);
        validateTokens();
        isOperationValid(input);
    }
}
