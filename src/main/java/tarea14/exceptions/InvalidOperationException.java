package tarea14.exceptions;

public class InvalidOperationException extends RuntimeException{
    public InvalidOperationException(String expression){
        super("The expression: " + expression + " is not a valid operation");
    }
}
