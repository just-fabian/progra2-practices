package tarea14.exceptions;

public class InvalidTokenException extends RuntimeException{
    public InvalidTokenException(char c){
        super("Invalid token: " + c);
    }
}
