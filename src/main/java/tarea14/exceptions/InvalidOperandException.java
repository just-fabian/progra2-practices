package tarea14.exceptions;

public class InvalidOperandException extends RuntimeException{
    public InvalidOperandException(){
        super("An operand is beyond the integer limit.");
    }
}
