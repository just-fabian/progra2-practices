package tarea14.exceptions;

public class InvalidResultException extends RuntimeException{
    public InvalidResultException(){
        super("The result value is beyond integer limit.");
    }
}
