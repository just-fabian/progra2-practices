package tarea14.operations;

import tarea14.exceptions.InvalidResultException;

public class Pow implements Operation{
    @Override
    public int calculate(int a, int b) {
        int result = 1;
        for(int i = 1; i<= b; i++){
            result = result * a;
        }
        if (result == 1 && (b != 1 && b != 0)) throw new InvalidResultException();
        return result;
    }

    @Override
    public int getPriority() {
        return 3;
    }

    @Override
    public char getOperator() {
        return '^';
    }

    @Override
    public int compareTo(Operation o) {
        return 0;
    }
}
