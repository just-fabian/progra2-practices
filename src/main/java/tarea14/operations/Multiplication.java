package tarea14.operations;

import tarea14.exceptions.InvalidResultException;

public class Multiplication implements Operation{
    @Override
    public int calculate(int a, int b) {
        int result = a * b;
        if (result < 0) throw new InvalidResultException();
        return result;
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    public char getOperator() {
        return '*';
    }

    @Override
    public int compareTo(Operation o) {
        return 0;
    }
}
