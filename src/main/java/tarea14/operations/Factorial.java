package tarea14.operations;

import tarea14.exceptions.InvalidResultException;

public class Factorial implements Operation{
    @Override
    public int calculate(int a, int b) {
        int result = 1;
        for (int i = 1; i < b+1; i++) {
            result *= i;
        }
        if (result == 0) throw new InvalidResultException();
        return result;
    }

    @Override
    public int getPriority() {
        return 4;
    }

    @Override
    public char getOperator() {
        return '!';
    }

    @Override
    public int compareTo(Operation o) {
        return 0;
    }
}
