package tarea14.operations;

public interface Operation extends Comparable<Operation> {
    int calculate(int a, int b);
    int getPriority();
    char getOperator();
}
