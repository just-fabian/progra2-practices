package tarea14;
import tarea13.Stack;
import tarea14.operations.*;

public class Calculator {
    private Stack<Character> operators;
    private Stack<Integer> operands;
    private Stack<Operation> operations;
    private Validator validator;

    public Calculator() {
        operators = new Stack<>();
        operands = new Stack<>();
        operations = new Stack<>();
        operations.push(new Factorial());
        operations.push(new Pow());
        operations.push(new Addition());
        operations.push(new Multiplication());
        validator = new Validator(operations);
    }

    private boolean isDigit(char token) {
        return Character.isDigit(token);
    }

    private boolean isOperator(char token) {
        return getCurrentOperator(token) != null;
    }

    private int getPriority(char operator) {
        Operation operation = getCurrentOperator(operator);
        return operation != null ? operation.getPriority() : 0;
    }

    private Operation getCurrentOperator(char operator){
        for (int i = 0; i < operations.size(); i++)
            if (operations.get(i).getOperator() == operator) return operations.get(i);
        return null;
    }

    private void processOperator(char operator) {
        int a = 0, b;

        b = operands.peek();
        operands.pop();
        if (operator != '!'){
            a = operands.peek();
            operands.pop();
        }
        Operation operation = getCurrentOperator(operator);
        int result = operation != null ? operation.calculate(a,b) : 0;
        operands.push(result);
    }

    private int addOperand(int i, int current, String input, char c){
        current = Integer.parseInt(current + "" + c);
        if(!(i < input.length() - 1) || !isDigit(input.charAt(i + 1))){
            operands.push(current);
            current = 0;
        }
        return current;
    }

    private void addOperator(char c){
        if (operators.isEmpty() || getPriority(c) > getPriority(operators.peek())) {
            operators.push(c);
        } else {
            while (!operators.isEmpty() && getPriority(c) <= getPriority(operators.peek())) {
                char toProcess = operators.peek();
                operators.pop();
                processOperator(toProcess);
            }
            operators.push(c);
        }
    }

    private void keepProcessing(){
        while (!operators.isEmpty() && isOperator(operators.peek())) {
            char toProcess = operators.peek();
            operators.pop();
            processOperator(toProcess);
        }
    }

    private void processExpression(String input) {
        int current = 0;
        for (int i = 0; i < input.length(); i++){
            char c = input.charAt(i);
            if (isDigit(c)) current = addOperand(i, current, input, c);
            else if (isOperator(c)) addOperator(c);
            else if (c == '(') operators.push(c);
            else if (c == ')') {
                while (!operators.isEmpty() && isOperator(operators.peek())) {
                    char toProcess = operators.peek();
                    operators.pop();
                    processOperator(toProcess);
                }
                if (!operators.isEmpty() && operators.peek() == '(')
                    operators.pop();
            }
        }

        keepProcessing();
    }

    public int calc(String input){
        validator.validateOperation(input);
        processExpression(input);
        int result = operands.peek();
        operands.pop();
        return result;
    }
}
