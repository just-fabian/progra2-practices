package tarea11;

public class DoublyLinkedList<T extends Comparable<T>> implements List<T>{
    private int size;

    private Node<T> head;

    @Override
    public void reverse(){
        Node<T> aux = null;
        Node<T> current = head;

        while (current != null){
            aux = current.getPrev();
            current.setPrev(current.getNext());
            current.setNext(aux);
            current = current.getPrev();
        }

        if (aux != null) head = aux.getPrev();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public T get(int index) {
        if (!isValidIndex(index)) return null;
        Node<T> node = head;
        for (int i = 0; i < index; i++, node = node.getNext());
        return node.getData();
    }

    @Override
    public void selectionSort() {
        for (int i = 0; i < size - 1; i++) {
            int indexMin = i;
            for (int j = i + 1; j < size; j++)
                if (get(indexMin).compareTo(get(j)) > 0) indexMin = j;
            swapNodes(indexMin, i);
        }
    }

    @Override
    public void bubbleSort() {
        boolean sorted;
        do {
            sorted = true;
            for (int i = 0; i < size - 1; i++) {
                if (get(i).compareTo(get(i + 1)) > 0) {
                    swapNodes(i, i + 1);
                    sorted  = false;
                }
            }
        } while (!sorted);
    }

    private boolean isValidIndex(int index){
        return !isEmpty() && index < size && index >= 0;
    }

    private void swapNodes(int y, int x) {
        if (y != x){
            Node<T> prevY = null, currY = head;
            for (int i = 0; i != y; i++, prevY = currY, currY = currY.getNext());

            Node<T> prevX = null, currX = head;
            for (int i = 0; i != x; i++, prevX = currX, currX = currX.getNext());

            Node<T> currXPrev = currX.getPrev() != null ? currX.getPrev() : null;

            if (prevY != null){
                prevY.setNext(currX);
                currX.setPrev(currY.getPrev());
                if (currX.getNext() != null) currX.getNext().setPrev(currY);
                else currX.setPrev(prevY);
            }
            else{
                head = currX;
                head.setPrev(null);
            }

            if (prevX != null){
                prevX.setNext(currY);
                currY.setPrev(currXPrev);
                if (currY.getNext() != null) currY.getNext().setPrev(currX);
                else currY.setPrev(prevX);
            }
            else{
                head = currY;
                head.setPrev(null);
            }

            Node<T> temp = currY.getNext();
            currY.setNext(currX.getNext());
            currX.setNext(temp);
        }
    }

    @Override
    public boolean add(T data) {
        Node<T> newNode = new Node<>(data);
        newNode.setNext(null);
        newNode.setPrev(null);
        if(head == null) {
            head = newNode;
        } else {
            Node<T> temp = head;
            while(temp.getNext() != null)
                temp = temp.getNext();
            temp.setNext(newNode);
            newNode.setPrev(temp);
        }
        size++;

        return true;
    }

    @Override
    public boolean remove(T data) {
        return false;
    }

    @Override
    public void mergeSort() {

    }

    public Object[] toArray() {
        Node<T> node = head;
        Object[] nodeToArray = new Object[size()];
        int counter = 0;
        while (node != null) {
            nodeToArray[counter] = node.getData();
            counter ++;
            node = node.getNext();
        }
        return nodeToArray;
    }

    public String toString() {
        return "(" + size + ") head: " + head;
    }
}
