package tarea11;

public interface List<T extends Comparable<T>> {
    int size();

    boolean isEmpty();

    boolean add(T data);

    boolean remove(T data);

    T get(int index);

    void selectionSort();

    void bubbleSort();

    void reverse();

    void mergeSort();

    Object[] toArray();
}
