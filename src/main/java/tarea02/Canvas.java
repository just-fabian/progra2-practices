package tarea02;

import java.util.Arrays;

/**
 * Simple drawing board kata class
 * Instructions:
 * You are going to write a simple console-style drawing board program.
 *
 * Functions
 * The drawing board is very simple so only these functions are supported.
 *
 * Function	Description
 * Create canvas	Create new empty canvas (filled with ' ') with given width and height
 * Draw line / rectangle	Draw the line with 'x' which connects the given points.
 * If the points are diagonal, a rectangle instead of diagonal line should be drawn
 * Fill color	Fill the entire area connected to the given point with given character,
 * also known as "bucket fill" in paint programs
 *
 * Output
 * Implement the drawCanvas method to return the string contains the entire canvas surrounded by
 * borders with - and |.
 *
 * Error handling: should throw java.lang.IllegalArgumentException
 *
 * @author Fabián Romero Claros
 */
public class Canvas {
    /**
     * Width and height for the board
     */
    private int width, height;
    /**
     * A char matrix with values drawn in the board
     */
    private char[][] valuesDrawnInBoard;
    /**
     * Values to draw the inner rectangle in the board
     */
    private int x1, x2, y1, y2;
    /**
     * A string builder to draw the board
     */
    private StringBuilder boardStringBuilder;

    /**
     * Constants to draw the board
     */
    private final static char CHAR_EMPTY_SPACE = ' ',
        CHAR_BOARD_HORIZONTAL_BORDER = '-',
        CHAR_BOARD_VERTICAL_BORDER = '|',
        CHAR_INNER_RECTANGLE_BORDER = 'x';

    /**
     * Class constructor.
     * It sets the width and height board, initializes the boardStringBuilder
     * and sets all the values of valuesDrawnInBoard as CHAR_EMPTY_SPACE
     * @param width the width board
     * @param height the height board
     */
    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;

        boardStringBuilder = new StringBuilder();

        valuesDrawnInBoard = new char[height][width];
        for (int i = 0; i < height; i++) {
            Arrays.fill(valuesDrawnInBoard[i], CHAR_EMPTY_SPACE);
        }
    }

    /**
     * This method draws the inner rectangle horizontal borders with a sequence of CHAR_INNER_RECTANGLE_BORDER
     */
    private void drawInnerRectangleHorizontalBorder() {
        for (int i = x1; i <= x2; i++) {
            valuesDrawnInBoard[y1][i] = CHAR_INNER_RECTANGLE_BORDER;
            valuesDrawnInBoard[y2][i] = CHAR_INNER_RECTANGLE_BORDER;
        }
    }

    /**
     * This method draws the inner rectangle vertical borders with a sequence of CHAR_INNER_RECTANGLE_BORDER
     */
    private void drawInnerRectangleVerticalBorder() {
        for (int i = y1; i <= y2; i++) {
            valuesDrawnInBoard[i][x1] = CHAR_INNER_RECTANGLE_BORDER;
            valuesDrawnInBoard[i][x2] = CHAR_INNER_RECTANGLE_BORDER;
        }
    }

    /**
     * This method draws the inner rectangle
     * It calls two methods to draw the rectangle borders
     */
    private void drawInnerRectangle() {
        drawInnerRectangleHorizontalBorder();
        drawInnerRectangleVerticalBorder();
    }

    /**
     * This method draws the inner rectangle in board
     * @param x1 start inner rectangle x coordinate
     * @param y1 start inner rectangle y coordinate
     * @param x2 end inner rectangle x coordinate
     * @param y2 end inner rectangle y coordinate
     * @return this class
     */
    public Canvas draw(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        drawInnerRectangle();
        return this;
    }

    /**
     * This method draws the horizontal border of the board appending to the boardStringBuilder
     * a sequence of CHAR_BOARD_HORIZONTAL_BORDER
     * @param isFirstBorder if it is the up border, it will append a line break
     */
    private void drawBoardHorizontalBorder(boolean isFirstBorder){
        for (int i = 0; i < width + 2; i++){
            boardStringBuilder.append(CHAR_BOARD_HORIZONTAL_BORDER);
        }
        if (isFirstBorder) boardStringBuilder.append('\n');
    }

    /**
     * This method draws the board.
     * It will append the board and inner rectangle borders to the boardStringBuilder
     * @return the boardStringBuilder as string
     */
    public String drawCanvas() {
        drawBoardHorizontalBorder(true);

        for (int i = 0; i < height; i++) {
            boardStringBuilder.append(CHAR_BOARD_VERTICAL_BORDER);
            for (int j = 0; j < width; j++) {
                boardStringBuilder.append(valuesDrawnInBoard[i][j]);
            }
            boardStringBuilder.append(CHAR_BOARD_VERTICAL_BORDER).append('\n');
        }
        drawBoardHorizontalBorder(false);


        return boardStringBuilder.toString();
    }
}