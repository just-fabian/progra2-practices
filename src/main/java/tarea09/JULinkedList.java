package tarea09;
import java.util.*;

public class JULinkedList<T> implements List<T> {
    private int size;
    private Node<T> head;

    private Node<T> getNode(int index){
        if (!isValidIndex(index)) return null;
        Node<T> node = head;
        for (int i = 0; i < index; i++, node = node.getNext());
        return node;
    }

    private boolean isValidIndex(int index){
        return !isEmpty() && index < size && index >= 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            private int counter = 0;
            private Node<T> current = head;
            public boolean hasNext() {
                return size > counter;
            }
            public T next() {
                if (counter > 0) current = current.getNext();
                counter++;
                return current.getData();
            }
        };
    }

    @Override
    public boolean add(T element) {
        Node<T> node = new Node<>(element);
        if (isEmpty()) head = node;
        else {
            Node<T> last = getNode(size - 1);
            last.setNext(node);
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (
                Node<T> current = head, previous = head;
                current != null;
                previous = current, current = current.getNext()
        ) {
            if (current.getData().equals(o)) {
                if (head == current) head = current.getNext();
                else previous.setNext(current.getNext());
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public boolean contains(Object o) {
        for (Node<T> node = head; node != null; node = node.getNext())
            if (node.getData().equals(o)) return true;
        return false;
    }

    @Override
    public T get(int index) {
        if (!isValidIndex(index)) return null;
        Node<T> node = getNode(index);
        return node == null ? null : node.getData();
    }

    @Override
    public T remove(int index) {
        if (isValidIndex(index)){
            Node<T> temp = head;
            T removedData;
            if (index == 0) {
                removedData = head.getData();
                head = head.getNext();
            } else{
                for (int i = 0; temp != null && i < index - 1; i++, temp = temp.getNext());
                removedData = temp.getNext().getData();
                temp.setNext(temp.getNext().getNext());
            }
            size--;

            return removedData;
        }

        return null;
    }

    @Override
    public T set(int index, T element) {
        if (isValidIndex(index)){
            Node<T> node = getNode(index);
            T previousData = node.getData();
            node.setData(element);
            return previousData;
        }
        return null;
    }

    @Override
    public void add(int index, T element) {
        if (isEmpty() && index == 0){
            head = new Node<>(element);
            size++;
        }
        else if (isValidIndex(index)){
            Node<T> current = head;
            Node<T> newNode = new Node<>(element);

            if (index == 0){
                newNode.setNext(head);
                head = newNode;
            } else {
                for (int i = 0; i < index - 1; i++) {
                    current = current.getNext();
                }
                newNode.setNext(current.getNext());
                current.setNext(newNode);
            }
            size++;
        }
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        for (int i = 0; i < array.length; i++) array[i] = get(i);

        return array;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        List<T> subList = new ArrayList<>();
        if (
            fromIndex < 0 || toIndex >= size || fromIndex > toIndex
        ) System.out.println("No valid indexes");
        else for (int i = fromIndex; i < toIndex; i++) subList.add(get(i));

        return subList;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1;
        for (int i = 0; i < size; i++){
            if (o.equals(get(i))) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = -1;
        for (int i = size - 1; i >= 0; i--){
            if (o.equals(get(i))){
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + head;
    }
}
