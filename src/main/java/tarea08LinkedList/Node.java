package tarea08LinkedList;

public class Node {
    int data;
    Node next = null;

    public Node(){}

    Node(final int data) {
        this.data = data;
    }

    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    public static int getNth(Node n, int index) throws Exception{
        if(n == null || index < 0) throw new Exception("Not valid arguments");

        if(index == 0) return n.data;
        return getNth(n.next, index-1);
    }

    public static Node append(Node listA, Node listB) {
        Node tmp = listA;
        if (listA == null || listB == null) return listA == null ? listB : listA;

        while(tmp.next != null) {
            tmp = tmp.next;
        }
        tmp.next = listB;

        return listA;
    }
}
