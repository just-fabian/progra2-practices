package clase0812;

public interface List<T> {
    int size();
    boolean isEmpty();
    boolean add(T data);
    boolean remove(T data);
    T get(int index);

}

