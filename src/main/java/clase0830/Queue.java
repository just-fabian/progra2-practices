package clase0830;

public class Queue {
    private Node head;
    private Node tail;
    private int size;

    public void enqueue(int data){
        Node newNode = new Node(data);

        if (size == 0){
            head = newNode;
        } else{
            newNode.setNext(tail);
        }

        tail = newNode;
        size++;
    }

    public int deque(){
        if (size == 0) return -1;
        Node node = tail;
        Node node1 = tail;
        while (node != head){
            node = node.getNext();
            if (node1.getNext() != head) node1 = node1.getNext();
        }
        node1.setNext(null);
        head = node1;
        size--;
        return node.getData();
    }

    public String toString(){
        return "(" + size + ")" + "  head: " + head + "\n tail: " + tail;
    }
}
