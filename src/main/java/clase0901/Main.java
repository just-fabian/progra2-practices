package clase0901;

import clase0830.Queue;

public class Main {
    public static void main(String[] args) {
        Queue queue1 = new Queue();
        queue1.enqueue(1);
        queue1.enqueue(3);
        queue1.enqueue(5);

        Queue queue2 = new Queue();
        queue2.enqueue(2);
        queue2.enqueue(4);
        queue2.enqueue(6);

        Queue queue = new Queue();
        int i = queue1.deque();
        int j = queue2.deque();

        while (i != -1 && j != -1){
            queue.enqueue(i);
            queue.enqueue(j);

            i = queue1.deque();
            j = queue2.deque();
        }

        System.out.println(queue);

    }
}
