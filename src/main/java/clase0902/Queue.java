package clase0902;

public class Queue<T extends Comparable<T>> {
    private Node<T> head;
    private Node<T> tail;
    private int size;

    public void enqueue(T data){
        Node<T> current = tail;
        while (current != null){
            if (current.getData().compareTo(data) == 0) return;
            current = current.getNext();
        }

        Node<T> newNode = new Node<>(data);
        if (size == 0){
            head = newNode;
        } else{
            newNode.setNext(tail);
        }

        tail = newNode;
        size++;
    }

    public T dequeue(){
        Node<T> node = tail;
        Node<T> node1 = tail;
        while (node != head){
            node = node.getNext();
            if (node1.getNext() != head) node1 = node1.getNext();
        }
        node1.setNext(null);
        head = node1;
        size--;
        return node.getData();
    }

    public String toString(){
        return "(" + size + ")" + "  head: " + head + "\n tail: " + tail;
    }
}
