package clase0902;


public class Main {
    public static void main(String[] args) {
        Queue<Integer> queueInt = new Queue<>();
        queueInt.enqueue(1);
        queueInt.enqueue(1);
        queueInt.enqueue(2);
        queueInt.enqueue(3);
        queueInt.enqueue(3);
        queueInt.enqueue(4);

        queueInt.enqueue(1);
        queueInt.enqueue(2);

        System.out.println(queueInt);

        Queue<String> queueStr = new Queue();
        queueStr.enqueue("a");
        queueStr.enqueue("a");
        queueStr.enqueue("b");
        queueStr.enqueue("b");
        queueStr.enqueue("c");
        queueStr.enqueue("d");
        queueStr.enqueue("d");

        queueStr.enqueue("b");
        queueStr.enqueue("c");

        System.out.println(queueStr);
    }
}
