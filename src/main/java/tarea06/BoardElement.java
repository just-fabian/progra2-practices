package tarea06;

public abstract class BoardElement {
    protected int positionX, positionY;
    protected final String VALUE_IN_BOARD;

    public BoardElement(String VALUE_IN_BOARD){
        this.VALUE_IN_BOARD = VALUE_IN_BOARD;
    }

    public BoardElement(String VALUE_IN_BOARD, int positionY, int positionX){
        this.VALUE_IN_BOARD = VALUE_IN_BOARD;
        this.positionY = positionY;
        this.positionX = positionX;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public String getValue_in_board() {
        return VALUE_IN_BOARD;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public void setCoordinates(int y, int x) {
        this.positionY = y;
        this.positionX = x;
    }

    public abstract boolean isEmpty();
}
