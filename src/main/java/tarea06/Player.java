package tarea06;

public class Player extends BoardElement {
    public Player(String name){
        super(name);
    }

    public Player(String name, int y, int x) {
        super(name, y, x);
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) return true;

        if(!(obj instanceof Player)) return false;

        Player player = (Player) obj;
        return this.VALUE_IN_BOARD.equals(player.VALUE_IN_BOARD);
    }

    private void makeMovement(BoardElement[][] board, int newPosY, int newPosX){
        board[this.positionY][this.positionX] = board[newPosY][newPosX];
        board[this.positionY][this.positionX].setCoordinates(this.positionY, this.positionX);
        board[newPosY][newPosX] = this;
    }

    public void moveRight(BoardElement[][] board){
        makeMovement(board, this.positionY, this.positionX + 1);
        this.positionX++;
    }

    public void moveDown(BoardElement[][] board){
        makeMovement(board, this.positionY + 1, this.positionX);
        this.positionY++;
    }

    public void moveLeft(BoardElement[][] board){
        makeMovement(board, this.positionY, this.positionX - 1);
        this.positionX--;
    }

    public void moveUp(BoardElement[][] board){
        makeMovement(board, this.positionY - 1, this.positionX);
        this.positionY--;
    }
}
