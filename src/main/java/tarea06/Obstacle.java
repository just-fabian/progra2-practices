package tarea06;

public class Obstacle extends BoardElement {
    private static final String OBSTACLE_VALUE = "O";

    public Obstacle() {
        super(OBSTACLE_VALUE);
    }

    public Obstacle(int posX, int poxY) {
        super(OBSTACLE_VALUE, poxY, posX);
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
