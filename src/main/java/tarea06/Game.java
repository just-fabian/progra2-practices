package tarea06;

public class Game {
    protected BoardElement[][] board = new BoardElement[10][10];
    protected Obstacle[] obstacles;

    protected Player[] players;

    public Game(int playersNumber, int obstaclesNumber) {
        fill();
        setObstacles(obstaclesNumber);
        players = new Player[playersNumber];
    }

    public void addPlayer(Player player){
        for (int i = 0; i < players.length; i++) {
            if (players[i] == null) {
                players[i] = player;
                setInBoard(player);
                break;
            } else if (player.equals(players[i]))  break;
        }
    }

    private int searchIndexPlayer(Player player){
        for (int i = 0; i < players.length; i++) {
            if (player.equals(players[i])) return i;
        }
        return -1;
    }

    private void fill(){
        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board[i].length; j++){
                board[i][j] = new EmptySpace(i, j);
            }
        }
    }

    private void setInBoard(BoardElement item){
        int randomRow, randomCol;
        do{
            randomRow = (int)(Math.random()*10);
            randomCol = (int)(Math.random()*10);

            if (board[randomRow][randomCol].isEmpty() && (randomRow != 9 && randomCol != 9)) {
                board[randomRow][randomCol] = item;
                item.setCoordinates(randomRow, randomCol);
            }
        } while (board[randomRow][randomCol].isEmpty());
    }

    private void setObstacles(int obstaclesNumber){
        obstacles = new Obstacle[obstaclesNumber];
        for (int i = 0; i < obstacles.length; i++){
            obstacles[i] = new Obstacle();
            setInBoard(obstacles[i]);
        }
    }

    public boolean posibleMove(int y, int x){
        return board[y][x].isEmpty();
    }

    public void movePlayerRight(Player pl) {
        Player player = players[searchIndexPlayer(pl)];
        if (player.getPositionX() != 9 && posibleMove(player.getPositionY(), player.getPositionX() + 1)) {
            player.moveRight(board);
        } else System.out.println("Not possible movement");
    }

    public void movePlayerDown(Player pl){
        Player player = players[searchIndexPlayer(pl)];
        if (player.getPositionY() != 9 && posibleMove(player.getPositionY() + 1, player.getPositionX())){
            player.moveDown(board);
        } else System.out.println("Not possible movement");
    }

    public void movePlayerLeft(Player pl){
        Player player = players[searchIndexPlayer(pl)];
        if(player.getPositionX() != 0 && posibleMove(player.getPositionY(), player.getPositionX() - 1)){
            player.moveLeft(board);
        } else System.out.println("Not possible movement");
    }

    public void movePlayerUp(Player pl){
        Player player = players[searchIndexPlayer(pl)];
        if(player.getPositionY() != 0 && posibleMove(player.getPositionY() - 1, player.getPositionX())){
            player.moveUp(board);
        } else System.out.println("Not possible movement");
    }

    public String printBoard() {
        String boardPrinted = "";
        for(int i = 0; i < board.length; i++){
            boardPrinted += " | ";
            for (int j = 0; j < board[i].length; j++){
                boardPrinted += board[i][j].getValue_in_board() + " , ";
            }
            boardPrinted += " | \n";
        }
        return boardPrinted;
    }
}
