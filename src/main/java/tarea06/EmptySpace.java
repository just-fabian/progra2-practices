package tarea06;

public class EmptySpace extends BoardElement {
    private static final String EMPTY_VALUE = " ";

    public EmptySpace(int y, int x) {
        super(EMPTY_VALUE, y, x);
    }

    @Override
    public boolean isEmpty() {
        return true;
    }
}
