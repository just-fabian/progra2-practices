package clase0809;

class Nodo {
    public int dato;
    public Nodo sig;
    public Nodo(int dato) { this.dato = dato;}
    public String toString() {
        return "{dato=" + dato + ", sig->" + sig + "}";
    }
}

class Lista {
    public Nodo raiz;
    // remover
    void agregar(int dato) {
        Nodo nodo = new Nodo(dato);
        nodo.sig = raiz;
        raiz = nodo;
    }
    void remover(int dato) {
        /* Nodo nodo = raiz;
           Nodo nodo2 = raiz;
           while (nodo != null) {
            if (nodo.dato == dato) {
                if (nodo == raiz)
                    raiz = nodo.sig;
                else {
                    nodo2.sig = nodo.sig;
                }
                break;
            }
            nodo2 = nodo;
            nodo = nodo.sig;
        }*/

        for(Nodo nodo = raiz, nodo2 = raiz; nodo != null; nodo2 = nodo, nodo = nodo.sig) {
            if (nodo.dato == dato) {
                if (nodo == raiz) raiz = nodo.sig;
                else nodo2.sig = nodo.sig;
                break;
            }
        }
    }

    public String toString() {
        return "raiz=" + raiz;
    }
}

public class Main {
    public static void main(String[] args) {
        /*Nodo n1 = null;
        //System.out.println("n1: " + n1);
        n1 = new Nodo(1);
        //System.out.println("n1: " + n1);
        Nodo n2 = new Nodo(2);
        //System.out.println("n2: " + n2);
        n1.sig = n2;
        System.out.println("n1: " + n1);*/
        Lista lista = new Lista();
        lista.agregar(1);
        System.out.println("lista: " + lista);
        lista.agregar(2);
        System.out.println("lista: " + lista);
        lista.agregar(3);
        System.out.println("lista: " + lista);
        lista.agregar(4);
        System.out.println("lista: " + lista);
        lista.agregar(5);
        System.out.println("lista: " + lista);
        lista.remover(1);
        System.out.println("lista: " + lista);
        lista.remover(5);
        System.out.println("lista: " + lista);
        lista.remover(3);
        System.out.println("lista: " + lista);
    }
}