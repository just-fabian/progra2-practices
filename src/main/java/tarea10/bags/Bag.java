package tarea10.bags;

public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();
    /*
    change the x element position to y position and
    y element position to x position
    0 1 2 3 4
    1,2,3,4,5
    xchange(1,3) =>
    1,4,3,2,5
    * */
    void xchange(int y, int x);

}
class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}

class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;

    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    private T getNodeData(int index){
        Node<T> node = root;
        for (int i = 0; i < index; i++, node = node.getNext());
        return node.getData();
    }

    public  void selectionSort() {
        for (int i = 0; i < size - 1; i++) {
            int indexMin = i;
            for (int j = i + 1; j < size; j++)
                if (getNodeData(indexMin).compareTo(getNodeData(j)) > 0) indexMin = j;
            xchange(indexMin, i);
        }
    }

    public void bubbleSort() {
        boolean sorted;
        do {
            sorted = true;
            for (int i = 0; i < size - 1; i++) {
                if (getNodeData(i).compareTo(getNodeData(i + 1)) > 0) {
                    xchange(i, i + 1);
                    sorted  = false;
                }
            }
        } while (!sorted);
    }

    @Override
    public void xchange(int y, int x) {
        if (y != x){
            Node<T> prevY = null, currY = root;
            for (int i = 0; i != y; i++, prevY = currY, currY = currY.getNext());

            Node<T> prevX = null, currX = root;
            for (int i = 0; i != x; i++, prevX = currX, currX = currX.getNext());

            if (prevY != null) prevY.setNext(currX);
            else root = currX;

            if (prevX != null) prevX.setNext(currY);
            else root = currY;

            Node<T> temp = currY.getNext();
            currY.setNext(currX.getNext());
            currX.setNext(temp);
        }
    }
}