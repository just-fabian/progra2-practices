package tarea03;
import java.util.Arrays;

public class Kata {
    public int mostFrequentItemCount(int[] collection) {
        if (collection.length == 0) return 0;
        Arrays.sort(collection);

        int currentItemRepetitions = 1, maxItemRepetitions = 1;

        for(int i = 1; i < collection.length; i++){
            int previousItem = collection[i - 1];
            currentItemRepetitions = collection[i] == previousItem ? currentItemRepetitions + 1 : 1;
            if (currentItemRepetitions > maxItemRepetitions) maxItemRepetitions = currentItemRepetitions;
        }
        return maxItemRepetitions;
    }
}