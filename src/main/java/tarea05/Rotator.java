package tarea05;

/**
 * Create a method named "rotate" that returns a given array with the elements inside the array
 * rotated n spaces.
 *
 * If n is greater than 0 it should rotate the array to the right. If n is less than 0 it should rotate the
 * array to the left. If n is 0, then it should return the array unchanged.
 */
public class Rotator {
    private void rotateOneRight(Object[] data){
        Object lastItem = data[data.length-1];
        for(int j = data.length - 1; j > 0; j--){
            data[j] = data[j - 1];
        }

        data[0] = lastItem;
    }

    private void rotateOneLeft(Object[] data){
        Object firstItem = data[0];
        for (int j = 0; j < data.length - 1; j++) {
            data[j] = data[j + 1];
        }

        data[data.length - 1] = firstItem;
    }


    public Object[] rotate(Object[] data, int n) {
        int repetitions = n >= 0 ? n : n *- 1;
        for(int i = 0; i < repetitions; i++) {
            if(n > 0) rotateOneRight(data);
            else rotateOneLeft(data);
        }
        return data;
    }
}
