package tarea12;

import tarea12.recursividad.LlamadasEncadenadas;
import tarea12.recursividad.RecursividadvsIteracion;
import tarea12.recursividad.Infinito;
import tarea12.recursividad.Contador;

public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        LlamadasEncadenadas.doOne();

        // Descomentar la llamada a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuación describir el problema:
        // Descripción:.................
        /*
            DESCRIPCIÓN:
                El problema es que lo único que hace el método es llamarse a sí mismo, entonces
                este proceso se repetirá infinitamente, ya que no hay ninguna parte que especifique
                cuándo debería de parar este comportamiento.
                La exception StakOverFlowError se da, ya que al llamarse el método a sí mismo
                infinitamente, la memoria reservada en el stack se queda sin espacio
         */
        // 3.- volver a comentar la llamada a: Infinito.whileTrue()
//        Infinito.whileTrue();

        // Descomentar la llamada a: Contador.contarHasta(), pasando un número entero positivo
        // 1.- sacar screenshot de la salida del método
        // 2.- a continuación describir como funciona este método:
        /*
            DESCRIPCIÓN:
                Este método lo que hace es imprimir por consola el contador (el número pasado por parámetro)
                y se llamará a sí mismo mientras el contador sea mayor a 0, es decir
                dejará de llamarse cuando el contador sea igual a 0,
                por cada llamada el contador disminuirá en uno, es decir en el caso de abajo como le pasamos 7
                como contador, el método primero imprime este número, imprime 7, después verifica si este contador
                es mayor a 0, si se cumple la condición el método se llama así mismo con el contador reducido en 1,
                es decir se llama a sí mismo pero ahora pasándose 6, y vuelve a imprimir por consola este número y a
                verificar si es mayor a 0 para volver a llamarse con el contador reducido en uno y así sucesivamente
                esto se repetirá hasta que el contador sea igual a 0 para que no entre en la condición y el
                método ya no se llamará así mismo terminando el proceso
                Este método aplica el concepto de recursividad llamándose a sí mismo para realizar la misma acción
                 evitando el uso de bucles.
         */
        Contador.contarHasta(7);

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));
    }
}
