package tarea01.lombokBuilder;

/**
 * People class in lombok builder exercise.
 * Instruction: Encapsulate properly the class by providing read
 * accessors (setters are not required for this Kata).
 */
public class People{
    /**
     * private People attributes so it can only be accessed from this class
     */
    private int age;
    private String name;
    private String lastName;
    /**
     * GREET is final because the value is constant and is never going to change
     */
    private final String GREET = "hello";

    public String greet(){
        return GREET + " my name is " + name;
    }

    /**
     * This method gets the age value so that it can be read from another class in the project
     * @return the age attribute value
     */
    public int getAge() {
        return age;
    }

    /**
     * This method gets the name value so that it can be read from another class in the project
     * @return the name attribute value
     */
    public String getName() {
        return name;
    }

    /**
     * This method gets the lastname value so that it can be read from another class in the project
     * @return the lastname attribute value
     */
    public String getLastName() {
        return lastName;
    }
}