package tarea01.mysteryColors;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Mystery Colors exercise.
 * Create the implementation for the interface.
 * The implementation needs to be called "MysteryColorAnalyzerImpl".
 */
public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer {
    /**
     * This method overrides the numberOfDistinctColors from the interface implemented.
     * It returns the colors list parameter HashSet size
     * The HashSet is the collection where every item is unique so in this way
     * we can get the distinct colors number in the list
     * @param mysteryColors list of colors from which to determine the number of distinct colors
     * @return the number of distinct colors in list received from parameter
     */
    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        return new HashSet<>(mysteryColors).size();
    }

    /**
     * This method overrides the colorOccurrence from the interface implemented.
     * It returns the list size filtered where the color received from parameter
     * is equals to the current color iterated in the colors list received from parameter
     * so in this way we can get the color repetitions in the list.
     * @param mysteryColors list of colors from which to determine the count of a specific color
     * @param color color to count
     * @return the color occurrence number in the list
     */
    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        List<Integer> occurrences = new ArrayList<>();
        for(int i=0; i<mysteryColors.size(); i++){
            if(mysteryColors.get(i).equals(color)){
                occurrences.add(i);
            }
        }
        return occurrences.size();
    }
}
