package tarea13;

public class Stack<T extends Comparable<T>>{
    private Node<T> head;
    private int size;

    public Stack() {
        this.head = null;
        this.size = 0;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public T peek() {
        if (isEmpty()) return null;
        return head.getData();
    }

    public void push(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(head);
        head = node;
        size++;
    }

    public T pop(){
        if (!isEmpty()) {
            T dataDeleted = head.getData();
            head = head.getNext();
            size--;
            return dataDeleted;
        }
        return null;
    }

    public T get(int index) {
        Node<T> node = head;
        for (int i = 0; i < index; i++, node = node.getNext());
        return node.getData();
    }

    @Override
    public String toString() {
        return "(" + size + ") head: " + head;
    }
}
