package clase0810;

class Nodo<T> {
    T dato;
    Nodo<T> sig;
    public Nodo(T dato) { this.dato = dato;}
    public String toString() {
        return "{dato=" + dato + ", sig->" + sig + "}";
    }
}
public class ListaEnlazada<T> implements Lista<T> {
    private int longitud;
    private Nodo<T> raiz;
    public int longitud() {
        return longitud;
    }
    public boolean vacia() {
        return raiz == null;
    }
    public boolean contiene(T dato) {
        Nodo<T> nodo = obtenerNodo(dato);
        return nodo != null;
    }
    public boolean agregar(T dato) {
        Nodo<T> nodo = new Nodo<>(dato);
        if (vacia()) raiz = nodo;
        else {
            Nodo<T> ultimo = obtenerNodo(longitud - 1);
            ultimo.sig = nodo;
        }
        longitud++;
        return true;
    }
    public boolean remover(T dato) {
        Nodo<T> anterior = raiz;
        for (Nodo<T> actual = raiz; actual != null; anterior = actual, actual = actual.sig) {
            if (actual.dato.equals(dato)) {
                if (raiz == actual)
                    raiz = actual.sig;
                else
                    anterior.sig = actual.sig;
                longitud--;
            }
        }
        return false;
    }
    public boolean remover1(T dato) {
        if (vacia()) return false;
        if (raiz.dato.equals(dato)) {
            raiz = raiz.sig;
            longitud--;
            return true;
        }
        for (Nodo<T> anterior = raiz; anterior.sig != null; anterior = anterior.sig) {
            if (anterior.sig.dato.equals(dato)) {
                anterior.sig = anterior.sig.sig;
                longitud--;
                return true;
            }
        }
        return false;
    }
    public T obtener(int indice) {
        Nodo<T> nodo = obtenerNodo(indice);
        return nodo == null ? null : nodo.dato;
    }

    @Override
    public void invertir() {
        Nodo<T> anterior = null;
        Nodo<T> actual = raiz;
        while (actual != null) {
            Nodo<T> tmp = actual.sig;
            actual.sig = anterior;
            anterior = actual;
            actual = tmp;
            System.out.println(anterior);
        }
        raiz = anterior;
    }

    private Nodo<T> obtenerNodo(int indice) {
        if (vacia() || indice >= longitud)
            return null;
        Nodo<T> nodo = raiz;
        for (int i = 0; i < indice; i++, nodo = nodo.sig);
        return nodo;
    }
    private Nodo<T> obtenerNodo(T dato) {
        if (vacia())
            return null;
        for (Nodo<T> nodo = raiz; nodo != null; nodo = nodo.sig)
            if (nodo.dato.equals(dato)) return nodo;
        return null;
    }
    public String toString() {
        return "(" + longitud + ")" + "raiz=" + raiz;
    }
}
