package clase0810;

public class Main {
    public static void main(String[] args) {
        Lista<Integer> lista = new ListaEnlazada<>();
        lista.agregar(1);
        System.out.println("lista: " + lista);
        lista.agregar(2);
        System.out.println("lista: " + lista);
        lista.agregar(3);
        System.out.println("lista: " + lista);
        lista.agregar(4);
        System.out.println("lista: " + lista);
        lista.agregar(5);
        System.out.println("lista: " + lista + "\n");
        lista.invertir();
        System.out.println("lista: " + lista);
        System.out.println(lista.contiene(1) + " " + lista.contiene(2) + " " + lista.obtener(0));
        lista.remover(1);
        System.out.println("lista: " + lista);
        lista.remover(3);
        System.out.println("lista: " + lista);
        lista.remover(4);
        System.out.println("lista: " + lista);
        lista.remover(5);
        System.out.println("lista: " + lista);
        lista.remover(2);
        System.out.println("lista: " + lista);
//        System.out.println(lista.obtener(0));
//        System.out.println(lista.obtener(1));
//        System.out.println(lista.obtener(2));
//        System.out.println(lista.obtener(3));
//        System.out.println(lista.obtener(5));
    }
}