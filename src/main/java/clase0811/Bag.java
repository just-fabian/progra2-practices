package clase0811;

public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();
    void xchange(int firstIndex, int secondIndex);
}
class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }

    public void setData(T data) {
        this.data = data;
    }

    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}
class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;
        return true;
    }

    private Node<T> getNode(int index){
        Node<T> node = root;
        for (int i = 0; i < index; i++, node = node.getNext());
        return node;
    }

    public void set(int index, T element) {
        Node<T> node = getNode(index);
        node.setData(element);
    }

    public void xchange(int firstIndex, int secondIndex) {
        T first = getNode(firstIndex).getData();
        T second = getNode(secondIndex).getData();

        set(firstIndex, second);
        set(secondIndex, first);
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }
    public void selectionSort() {
    }
    public void bubbleSort() {
    }
}
