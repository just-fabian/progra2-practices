package nodos;

public class Nodo {
    public int dato;
    public Nodo siguiente;

    public Nodo(){
    }

    public Nodo(int dato){
        this.dato = dato;
    }

    public Nodo (int dato, Nodo nodo){
        this.dato = dato;
        this.siguiente = nodo;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }

    @Override
    public String toString() {
        return "Nodo = | dato : " + dato + ", sig: " + siguiente + " |";
    }

    public static void main(String[] args) {
        Nodo nodo1 = new Nodo(1);
        Nodo nodo2 = new Nodo(2);
        Nodo nodo3 = new Nodo(3);

        System.out.println("\u001B[33m" + nodo1 + "\u001B[0m");

        nodo1.setSiguiente(nodo2);
        System.out.println("\u001B[33m" + nodo1 + "\u001B[0m");

        nodo2.setSiguiente(nodo3);
        System.out.println("\u001B[33m" + nodo1 + "\u001B[0m");
    }
}
