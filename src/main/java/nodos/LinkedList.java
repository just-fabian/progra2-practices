package nodos;

public class LinkedList {
    // Creating a node
    Nodo head;

    public LinkedList(){}
    public LinkedList(Nodo head){
        this.head = head;
    }

    public void setHead(Nodo head) {
        this.head = head;
    }

    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();

        // Assign value values
        Nodo nodo1 = new Nodo(1);
        Nodo nodo2 = new Nodo(2);
        Nodo nodo3 = new Nodo(3);
        Nodo nodo4 = new Nodo(4);

        linkedList.setHead(nodo1);

        linkedList.head.siguiente = nodo2;
        nodo2.siguiente = nodo3;
        nodo3.siguiente = nodo4;

        while (linkedList.head != null) {
            System.out.print("\u001B[33m" + linkedList.head + "\u001B[0m");
            linkedList.head = linkedList.head.siguiente;
        }
    }
}
