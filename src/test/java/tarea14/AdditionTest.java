package tarea14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tarea14.exceptions.InvalidOperandException;
import tarea14.exceptions.InvalidResultException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AdditionTest {
    Calculator calc;

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }

    @Test
    public void twoOperandsSumTest() {
        int actual = calc.calc("1+1");
        int expected = 2;

        assertEquals(expected,actual);
    }

    @Test
    public void severalOperandsSumTest() {
        int actual = calc.calc("1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1");
        int expected = 32;

        assertEquals(expected,actual);
    }

    @Test
    public void largeIntsOperantsSumTest() {
        int actual = calc.calc("2000000+1");
        int expected = 2000001;

        assertEquals(expected,actual);
    }

    @Test
    public void validSpacesOperantsSumTest() {
        int actual = calc.calc("  1+1");
        int expected = 2;

        assertEquals(expected,actual);
    }

    @Test
    public void moreValidSpacesOperantsSumTest() {
        int actual = calc.calc(" 1  +  1 ");
        int expected = 2;

        assertEquals(expected,actual);
    }
    @Test
    public void severalOperantsAndSpacesSumTest() {
        int actual = calc.calc("  1  + 1   + 1+1  +1+1  +1+ 1+1+  1+1+1+1+1  +1 +1 +1 +1 +1 +1 +1 +1+1+1+1+1+1+1+1+1+1  +1   ");
        int expected = 32;

        assertEquals(expected,actual);
    }

    @Test
    public void severalLongOperantsAndSpacesSumTest() {
        int actual = calc.calc("  200  + 8906  + 1+2000  +10+32  +10+999  +7832   ");
        int expected = 19990;

        assertEquals(expected,actual);
    }

    @Test
    public void sumWithParentheses(){
        int actual = calc.calc("(1+1)");
        int expected = 2;

        assertEquals(expected,actual);
    }

    @Test
    public void sumWithParenthesesAndSpaces(){
        int actual = calc.calc("  (1  + 1)");
        int expected = 2;

        assertEquals(expected,actual);
    }

    @Test
    public void severalValuesSumWithParenthesesAndSpaces(){
        int actual = calc.calc("  (1  + 1) + (1 + 3) +  (2+3) + (9 + 8 + 7)");
        int expected = 35;

        assertEquals(expected,actual);
    }

    @Test
    public void severalLongValuesSumWithParenthesesAndSpaces(){
        int actual = calc.calc("  (193  + 210) + (34 + 63) +  (21+3) + (9903 + 80 + 70934)");
        int expected = 81441;

        assertEquals(expected,actual);
    }

    @Test
    public void beyondIntSum(){

        Exception exception = assertThrows(InvalidOperandException.class, () -> {
            calc.calc("2147483648 + 1");
        });

        String expectedMessage = "An operand is beyond the integer limit.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void beyondIntResult(){

        Exception exception = assertThrows(InvalidResultException.class, () -> {
            calc.calc("2147483647 + 1");
        });

        String expectedMessage = "The result value is beyond integer limit.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
}
