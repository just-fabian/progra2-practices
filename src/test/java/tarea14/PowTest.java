package tarea14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tarea14.exceptions.InvalidOperandException;
import tarea14.exceptions.InvalidResultException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PowTest {
    Calculator calc;

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }

    @Test
    public void twoOperandsPowTest() {
        int actual = calc.calc("4^3");
        int expected = 64;

        assertEquals(expected,actual);
    }

    @Test
    public void severalOperandsPowTest() {
        int actual = calc.calc("2^2^2^2");
        int expected = 256;

        assertEquals(expected,actual);
    }

    @Test
    public void largeIntsOperantsPowTest() {
        int actual = calc.calc("1000^3");
        int expected = 1000000000;

        assertEquals(expected,actual);
    }

    @Test
    public void validSpacesOperantsPowTest() {
        int actual = calc.calc("  2^3");
        int expected = 8;

        assertEquals(expected,actual);
    }

    @Test
    public void moreValidSpacesOperantsSumTest() {
        int actual = calc.calc(" 2  ^  3 ");
        int expected = 8;

        assertEquals(expected,actual);
    }
    @Test
    public void severalOperantsAndSpacesSumTest() {
        int actual = calc.calc("  2  ^ 2   ^ 2^2  ^2");
        int expected = 65536;

        assertEquals(expected,actual);
    }

    @Test
    public void longOperantsAndSpacesSumTest() {
        int actual = calc.calc("  20   ^ 10 ");
        int expected = 797966336;

        assertEquals(expected,actual);
    }

    @Test
    public void powWithParentheses(){
        int actual = calc.calc("(2^3)");
        int expected = 8;

        assertEquals(expected,actual);
    }

    @Test
    public void sumWithParenthesesAndSpaces(){
        int actual = calc.calc("  (2  ^ 3)");
        int expected = 8;

        assertEquals(expected,actual);
    }

    @Test
    public void severalValuesSumWithParenthesesAndSpaces(){
        int actual = calc.calc("  (2 ^   2) ^ (2    ^2)");
        int expected = 256;

        assertEquals(expected,actual);
    }

    @Test
    public void severalLongValuesSumWithParenthesesAndSpaces(){
        int actual = calc.calc("  (20 ^   2) ^  (3)");
        int expected = 64000000;

        assertEquals(expected,actual);
    }

    @Test
    public void zeroPow(){
        int actual = calc.calc("3^ 0");
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void beyondIntSum(){

        Exception exception = assertThrows(InvalidOperandException.class, () -> {
            calc.calc("2147483648 ^ 3");
        });

        String expectedMessage = "An operand is beyond the integer limit.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void beyondIntResult(){

        Exception exception = assertThrows(InvalidResultException.class, () -> {
            calc.calc("2147483647 ^ 2");
        });

        String expectedMessage = "The result value is beyond integer limit.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
}
