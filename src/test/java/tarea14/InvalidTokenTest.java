package tarea14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tarea14.exceptions.InvalidTokenException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InvalidTokenTest {
    Calculator calc;

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }

    @Test
    public void invalidOperator(){
        Exception exception = assertThrows(InvalidTokenException.class, () -> {
            calc.calc("2-2");
        });

        String expectedMessage = "Invalid token: -";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void invalidCharacter(){
        Exception exception = assertThrows(InvalidTokenException.class, () -> {
            calc.calc("2a2");
        });

        String expectedMessage = "Invalid token: a";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void invalidCharacterAtEnd(){
        Exception exception = assertThrows(InvalidTokenException.class, () -> {
            calc.calc("2*2a");
        });

        String expectedMessage = "Invalid token: a";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
}
