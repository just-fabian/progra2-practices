package tarea14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasicOperationsTest {
    Calculator calc;

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }

    @Test
    public void test1(){
        int actual = calc.calc("1+2+3");
        int expected = 6;

        assertEquals(expected, actual);
    }

    @Test
    public void test2(){
        int actual = calc.calc("2*2*5");
        int expected = 20;

        assertEquals(expected, actual);
    }

    @Test
    public void test3(){
        int actual = calc.calc("2^3+2");
        int expected = 10;

        assertEquals(expected, actual);
    }

    @Test
    public void test4(){
        int actual = calc.calc("1+2*5");
        int expected = 15;

        assertEquals(expected, actual);
    }

    @Test
    public void test5(){
        int actual = calc.calc("!5+10");
        int expected = 130;

        assertEquals(expected, actual);
    }

    @Test
    public void test6(){
        int actual = calc.calc("!3^2");
        int expected = 36;

        assertEquals(expected, actual);
    }

    @Test
    public void test7(){
        int actual = calc.calc("4^2*3");
        int expected = 48;

        assertEquals(expected, actual);
    }

    @Test
    public void test8(){
        int actual = calc.calc("3^!3+5*2");
        int expected = 1468;

        assertEquals(expected, actual);
    }

    @Test
    public void test9(){
        int actual = calc.calc("(1+2)+3");
        int expected = 6;

        assertEquals(expected, actual);
    }

    @Test
    public void test10(){
        int actual = calc.calc("2*(2*5)");
        int expected = 20;

        assertEquals(expected, actual);
    }

    @Test
    public void test11(){
        int actual = calc.calc("(2^3)+2");
        int expected = 10;

        assertEquals(expected, actual);
    }

    @Test
    public void test12(){
        int actual = calc.calc("2^(3+2)");
        int expected = 32;

        assertEquals(expected, actual);
    }

    @Test
    public void test13(){
        int actual = calc.calc("(1+2)*5");
        int expected = 15;

        assertEquals(expected, actual);
    }

    @Test
    public void test14(){
        int actual = calc.calc("1+(2*5)");
        int expected = 11;

        assertEquals(expected, actual);
    }

    @Test
    public void test15(){
        int actual = calc.calc("!(3 + 2)");
        int expected = 120;

        assertEquals(expected, actual);
    }

    @Test
    public void test16(){
        int actual = calc.calc("(!3)^2");
        int expected = 36;

        assertEquals(expected, actual);
    }

    @Test
    public void test17(){
        int actual = calc.calc("4^(2*3)");
        int expected = 4096;

        assertEquals(expected, actual);
    }

    @Test
    public void test18(){
        int actual = calc.calc("3^!3+(5*2)");
        int expected = 739;

        assertEquals(expected, actual);
    }
}
