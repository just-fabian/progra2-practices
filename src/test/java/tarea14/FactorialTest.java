package tarea14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tarea14.exceptions.InvalidOperandException;
import tarea14.exceptions.InvalidResultException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FactorialTest {
    Calculator calc;

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }

    @Test
    public void twoOperandFactorialTest() {
        int actual = calc.calc("!4");
        int expected = 24;

        assertEquals(expected,actual);

        actual = calc.calc("!1");
        expected = 1;

        assertEquals(expected,actual);
    }


    @Test
    public void largeIntsOperantFactorialTest() {
        int actual = calc.calc("!10");
        int expected = 3628800;

        assertEquals(expected,actual);
    }

    @Test
    public void moreValidSpacesOperantFactorialTest() {
        int actual = calc.calc(" !6 ");
        int expected = 720;

        assertEquals(expected,actual);
    }

    @Test
    public void longOperantAndSpacesFactorialTest() {
        int actual = calc.calc("  !  12   ");
        int expected = 479001600;

        assertEquals(expected,actual);
    }

    @Test
    public void factorialWithParentheses(){
        int actual = calc.calc("(!5)");
        int expected = 120;

        assertEquals(expected,actual);
    }

    @Test
    public void factorialWithParenthesesAndSpaces(){
        int actual = calc.calc("  (  !   5  ) ");
        int expected = 120;

        assertEquals(expected,actual);
    }

    @Test
    public void longValueFactorialWithParenthesesAndSpaces(){
        int actual = calc.calc("  ( !  13 )      ");
        int expected = 1932053504;

        assertEquals(expected,actual);
    }

    @Test
    public void zeroFactorial(){
        int actual = calc.calc("!0");
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void beyondIntFactorial(){

        Exception exception = assertThrows(InvalidOperandException.class, () -> {
            calc.calc("!2147483648");
        });

        String expectedMessage = "An operand is beyond the integer limit.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void beyondIntResult(){

        Exception exception = assertThrows(InvalidResultException.class, () -> {
            calc.calc("!100");
        });

        String expectedMessage = "The result value is beyond integer limit.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
}

