package tarea14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tarea14.exceptions.InvalidOperationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InvalidOperationExceptionTest {
    Calculator calc;

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }

    @Test
    public void twoOperatorsAtEnd(){
        String expression = "2**";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void twoOperatorsAtMiddle(){
        String expression = "2**2";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void operatorAtEnd(){
        String expression = "3+6^";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void spacesBetweenNumbers(){
        String expression = "3+6  3";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void noOperatorBeforeParentheses(){
        String expression = "6+3(3)";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void basicUnclosedParentheses(){
        String expression = "(3+6";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void missFirstParentheses(){
        String expression = "3+6)";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void unclosedParentheses1(){
        String expression = "(3+2+(3 + 4 + (3))";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void unclosedParentheses2(){
        String expression = "(3+2+(3 + 4 + (3))) + (3";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void unclosedParentheses3(){
        String expression = "((3+2+(3 + 4 + (3) + (4 + 4 + (4 * 9))) + (3)";
        Exception exception = assertThrows(InvalidOperationException.class, () -> {
            calc.calc(expression);
        });

        String expectedMessage = "The expression: " + expression + " is not a valid operation";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
}
