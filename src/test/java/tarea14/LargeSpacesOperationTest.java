package tarea14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LargeSpacesOperationTest {
    Calculator calc;

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }

    @Test
    public void test1(){
        int actual = calc.calc(" 131  +  221312  + 3231   ");
        int expected = 224674;

        assertEquals(expected, actual);
    }

    @Test
    public void test2(){
        int actual = calc.calc("  2324  *223   *  52  ");
        int expected = 26949104;

        assertEquals(expected, actual);
    }

    @Test
    public void test3(){
        int actual = calc.calc(" 2  ^   13  +    2123  ");
        int expected = 10315;

        assertEquals(expected, actual);
    }

    @Test
    public void test4(){
        int actual = calc.calc("113  +    212312   *   53   ");
        int expected = 11258525;

        assertEquals(expected, actual);
    }

    @Test
    public void test5(){
        int actual = calc.calc("  !  12  +  9232  ");
        int expected = 479010832;

        assertEquals(expected, actual);
    }

    @Test
    public void test6(){
        int actual = calc.calc("  !  13  ^  2 ");
        int expected = 1653604352;

        assertEquals(expected, actual);
    }

    @Test
    public void test7(){
        int actual = calc.calc(" 11  ^   3  *  31  ");
        int expected = 41261;

        assertEquals(expected, actual);
    }

    @Test
    public void test8(){
        int actual = calc.calc("  13  ^  ! 11+  2304 *21  ");
        int expected = 324493589;

        assertEquals(expected, actual);
    }

    @Test
    public void test9(){
        int actual = calc.calc("(   1212323 +  213213 )   + 31432  ");
        int expected = 1456968;

        assertEquals(expected, actual);
    }

    @Test
    public void test10(){
        int actual = calc.calc("  2123   *   (  22131  *   5132  )  ");
        int expected = 604299340;

        assertEquals(expected, actual);
    }

    @Test
    public void test11(){
        int actual = calc.calc(" (   3^12   ) +   2131342");
        int expected = 2662783;

        assertEquals(expected, actual);
    }
}
