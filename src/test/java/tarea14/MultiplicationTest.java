package tarea14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tarea14.exceptions.InvalidOperandException;
import tarea14.exceptions.InvalidResultException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MultiplicationTest {
    Calculator calc;

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }

    @Test
    public void twoOperandsMultiplicationTest() {
        int actual = calc.calc("4*3");
        int expected = 12;

        assertEquals(expected,actual);
    }

    @Test
    public void severalOperandsMultiplicationTest() {
        int actual = calc.calc("2*4*5*4*2*1*2*3*7");
        int expected = 13440;

        assertEquals(expected,actual);
    }

    @Test
    public void largeIntsOperantsMultiplicationTest() {
        int actual = calc.calc("2000000*3");
        int expected = 6000000;

        assertEquals(expected,actual);
    }

    @Test
    public void validSpacesOperantsMultiplicationTest() {
        int actual = calc.calc("  4* 3");
        int expected = 12;

        assertEquals(expected,actual);
    }

    @Test
    public void moreValidSpacesOperantsMultiplicationTest() {
        int actual = calc.calc(" 3  *  4 ");
        int expected = 12;

        assertEquals(expected,actual);
    }
    @Test
    public void severalOperantsAndSpacesMultiplicationTest() {
        int actual = calc.calc("  2  * 3   * 5*6 *1*8  *9* 3*2*     2   ");
        int expected = 155520;

        assertEquals(expected,actual);
    }

    @Test
    public void severalLongOperantsAndSpacesMultiplicationTest() {
        int actual = calc.calc("  200  * 32  * 1*1902   * 30");
        int expected = 365184000;

        assertEquals(expected,actual);
    }

    @Test
    public void sumWithParentheses(){
        int actual = calc.calc("(4*3)");
        int expected = 12;

        assertEquals(expected,actual);
    }

    @Test
    public void sumWithParenthesesAndSpaces(){
        int actual = calc.calc("  (4  *  3)");
        int expected = 12;

        assertEquals(expected,actual);
    }

    @Test
    public void severalValuesMultiplicationWithParenthesesAndSpaces(){
        int actual = calc.calc("  (4  * 2) * (1 * 3) *  (2*3) * (9 * 8 * 7)");
        int expected = 72576;

        assertEquals(expected,actual);
    }

    @Test
    public void severalLongValuesMultiplicationWithParenthesesAndSpaces(){
        int actual = calc.calc("  (193  * 210)  * (34   * 63)   ");
        int expected = 86815260;

        assertEquals(expected,actual);
    }

    @Test
    public void beyondIntMultiplication(){

        Exception exception = assertThrows(InvalidOperandException.class, () -> {
            calc.calc("2147483648 * 2");
        });

        String expectedMessage = "An operand is beyond the integer limit.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void beyondIntResult(){

        Exception exception = assertThrows(InvalidResultException.class, () -> {
            calc.calc("2147483647 * 2");
        });

        String expectedMessage = "The result value is beyond integer limit.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
}
