package tarea11;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DoublyLinkedListStringTest {
    List<String> doublyLinkedList;
    @BeforeEach
    public void init(){
        doublyLinkedList = new DoublyLinkedList<>();
    }

    @Test
    public void sizeTest(){
        assertEquals(0, doublyLinkedList.size());

        doublyLinkedList.add("red");
        assertEquals(1, doublyLinkedList.size());

        doublyLinkedList.add("blue");
        doublyLinkedList.add("orange");
        assertEquals(3, doublyLinkedList.size());

        doublyLinkedList = new DoublyLinkedList<>();
        assertEquals(0, doublyLinkedList.size());
    }

    @Test
    public void isEmptyTest(){
        assertTrue(doublyLinkedList.isEmpty());
        doublyLinkedList.add("uno");
        assertFalse(doublyLinkedList.isEmpty());

        doublyLinkedList.add("dos");
        doublyLinkedList.add("tres");
        assertFalse(doublyLinkedList.isEmpty());

        doublyLinkedList = new DoublyLinkedList<>();
        assertTrue(doublyLinkedList.isEmpty());

        doublyLinkedList.add("cero");
        assertFalse(doublyLinkedList.isEmpty());
    }

    @Test
    public void getTest(){
        assertNull(doublyLinkedList.get(0));

        doublyLinkedList.add("batman");
        assertEquals("batman", doublyLinkedList.get(0));

        assertNull(doublyLinkedList.get(1));
        assertNull(doublyLinkedList.get(-2));

        doublyLinkedList = new DoublyLinkedList<>();
        assertNull(doublyLinkedList.get(3));

        doublyLinkedList.add("flash");
        doublyLinkedList.add("wonder woman");
        doublyLinkedList.add("shazam");

        assertEquals("flash", doublyLinkedList.get(0));
        assertEquals("wonder woman", doublyLinkedList.get(1));
        assertEquals("shazam", doublyLinkedList.get(2));

        assertNull(doublyLinkedList.get(5));
    }

    @Test
    public void selectionSortTest(){
        doublyLinkedList.add("Bolivia");
        doublyLinkedList.add("Brasil");
        doublyLinkedList.add("Chile");
        doublyLinkedList.add("Ecuador");
        doublyLinkedList.add("Argentina");

        String[] array = new String[]{"Bolivia", "Brasil", "Chile", "Ecuador", "Argentina"};
        assertArrayEquals(array, doublyLinkedList.toArray());

        array = new String[]{"Argentina", "Bolivia", "Brasil", "Chile", "Ecuador"};
        doublyLinkedList.selectionSort();
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(5) head: null <- prev, data = Argentina, sig -> {Argentina <- prev, data = Bolivia, sig -> {Bolivia <- prev, data = Brasil, sig -> {Brasil <- prev, data = Chile, sig -> {Chile <- prev, data = Ecuador, sig -> null}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add("Colombia");
        assertEquals("Ecuador", doublyLinkedList.get(4));
        doublyLinkedList.selectionSort();
        assertEquals("Colombia", doublyLinkedList.get(4));
        assertEquals("Ecuador", doublyLinkedList.get(5));

        array = new String[]{"Argentina", "Bolivia", "Brasil", "Chile", "Colombia", "Ecuador"};

        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals("Argentina", doublyLinkedList.get(0));
        assertEquals("Bolivia", doublyLinkedList.get(1));
        assertEquals("Brasil", doublyLinkedList.get(2));
        assertEquals("Chile", doublyLinkedList.get(3));
        assertEquals("Colombia", doublyLinkedList.get(4));
        assertEquals("Ecuador", doublyLinkedList.get(5));

        assertEquals(
                "(6) head: null <- prev, data = Argentina, sig -> {Argentina <- prev, data = Bolivia, sig -> {Bolivia <- prev, data = Brasil, sig -> {Brasil <- prev, data = Chile, sig -> {Chile <- prev, data = Colombia, sig -> {Colombia <- prev, data = Ecuador, sig -> null}}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add("Venezuela");

        doublyLinkedList.add("Perú");
        assertEquals("Perú", doublyLinkedList.get(7));
        assertEquals("Venezuela", doublyLinkedList.get(6));

        doublyLinkedList.selectionSort();
        assertEquals("Perú", doublyLinkedList.get(6));
        assertEquals("Venezuela", doublyLinkedList.get(7));

        doublyLinkedList.add("Bolivia");
        assertEquals("Bolivia", doublyLinkedList.get(8));
        doublyLinkedList.selectionSort();
        assertEquals("Venezuela", doublyLinkedList.get(8));
        assertEquals("Bolivia", doublyLinkedList.get(1));
        assertEquals("Bolivia", doublyLinkedList.get(2));
    }

    @Test
    public void bubbleSortTest(){
        doublyLinkedList.add("Perú");
        doublyLinkedList.add("Argentina");
        doublyLinkedList.add("Uruguay");
        doublyLinkedList.add("Paraguay");
        doublyLinkedList.add("Bolivia");
        doublyLinkedList.add("Colombia");

        String[] array = new String[]{"Perú","Argentina","Uruguay","Paraguay","Bolivia","Colombia"};
        assertArrayEquals(array, doublyLinkedList.toArray());

        array = new String[]{"Argentina","Bolivia","Colombia","Paraguay","Perú","Uruguay"};
        doublyLinkedList.bubbleSort();
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(6) head: null <- prev, data = Argentina, sig -> {Argentina <- prev, data = Bolivia, sig -> {Bolivia <- prev, data = Colombia, sig -> {Colombia <- prev, data = Paraguay, sig -> {Paraguay <- prev, data = Perú, sig -> {Perú <- prev, data = Uruguay, sig -> null}}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add("Brasil");
        assertEquals("Colombia", doublyLinkedList.get(2));
        array = new String[]{"Argentina","Bolivia","Brasil","Colombia","Paraguay","Perú","Uruguay"};
        doublyLinkedList.bubbleSort();
        assertArrayEquals(array, doublyLinkedList.toArray());
        assertEquals("Brasil", doublyLinkedList.get(2));
        assertEquals("Colombia", doublyLinkedList.get(3));

        doublyLinkedList.add("Chile");
        array = new String[]{"Argentina","Bolivia","Brasil","Chile","Colombia","Paraguay","Perú","Uruguay"};
        doublyLinkedList.bubbleSort();
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals("Argentina", doublyLinkedList.get(0));
        assertEquals("Bolivia", doublyLinkedList.get(1));
        assertEquals("Brasil", doublyLinkedList.get(2));
        assertEquals("Chile", doublyLinkedList.get(3));
        assertEquals("Colombia", doublyLinkedList.get(4));
        assertEquals("Paraguay", doublyLinkedList.get(5));
        assertEquals("Perú", doublyLinkedList.get(6));
        assertEquals("Uruguay", doublyLinkedList.get(7));

        assertEquals(
                "(8) head: null <- prev, data = Argentina, sig -> {Argentina <- prev, data = Bolivia, sig -> {Bolivia <- prev, data = Brasil, sig -> {Brasil <- prev, data = Chile, sig -> {Chile <- prev, data = Colombia, sig -> {Colombia <- prev, data = Paraguay, sig -> {Paraguay <- prev, data = Perú, sig -> {Perú <- prev, data = Uruguay, sig -> null}}}}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add("Venezuela");
        assertEquals("Venezuela", doublyLinkedList.get(8));
        doublyLinkedList.bubbleSort();
        assertEquals("Venezuela", doublyLinkedList.get(8));
    }

    @Test
    public void reverseTest() {
        doublyLinkedList.add("a");
        doublyLinkedList.add("b");
        doublyLinkedList.add("c");
        doublyLinkedList.add("d");
        doublyLinkedList.add("e");

        String[] array = new String[]{"a","b","c","d","e"};
        assertArrayEquals(array, doublyLinkedList.toArray());

        doublyLinkedList.reverse();

        array = new String[]{"e","d","c","b","a"};
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(5) head: null <- prev, data = e, sig -> {e <- prev, data = d, sig -> {d <- prev, data = c, sig -> {c <- prev, data = b, sig -> {b <- prev, data = a, sig -> null}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.reverse();
        array = new String[]{"a","b","c","d","e"};
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(5) head: null <- prev, data = a, sig -> {a <- prev, data = b, sig -> {b <- prev, data = c, sig -> {c <- prev, data = d, sig -> {d <- prev, data = e, sig -> null}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add("f");
        doublyLinkedList.add("g");
        array = new String[]{"a","b","c","d","e","f","g"};
        assertArrayEquals(array, doublyLinkedList.toArray());

        doublyLinkedList.reverse();
        array = new String[]{"g","f","e","d","c","b","a"};
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(7) head: null <- prev, data = g, sig -> {g <- prev, data = f, sig -> {f <- prev, data = e, sig -> {e <- prev, data = d, sig -> {d <- prev, data = c, sig -> {c <- prev, data = b, sig -> {b <- prev, data = a, sig -> null}}}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.reverse();
        doublyLinkedList.reverse();
        assertArrayEquals(array, doublyLinkedList.toArray());
    }
}
