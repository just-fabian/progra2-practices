package tarea11;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DoublyLinkedListIntegerTest {
    List<Integer> doublyLinkedList;
    @BeforeEach
    public void init(){
        doublyLinkedList = new DoublyLinkedList<>();
    }

    @Test
    public void sizeTest(){
        assertEquals(0, doublyLinkedList.size());

        doublyLinkedList.add(1);
        assertEquals(1, doublyLinkedList.size());

        doublyLinkedList.add(2);
        doublyLinkedList.add(3);
        assertEquals(3, doublyLinkedList.size());

        doublyLinkedList = new DoublyLinkedList<>();
        assertEquals(0, doublyLinkedList.size());
    }

    @Test
    public void isEmptyTest(){
        assertTrue(doublyLinkedList.isEmpty());
        doublyLinkedList.add(2);
        assertFalse(doublyLinkedList.isEmpty());

        doublyLinkedList.add(3);
        doublyLinkedList.add(4);
        assertFalse(doublyLinkedList.isEmpty());

        doublyLinkedList = new DoublyLinkedList<>();
        assertTrue(doublyLinkedList.isEmpty());
    }

    @Test
    public void getTest(){
        assertNull(doublyLinkedList.get(0));

        doublyLinkedList.add(2);
        assertEquals(2, doublyLinkedList.get(0));

        assertNull(doublyLinkedList.get(1));
        assertNull(doublyLinkedList.get(-1));

        doublyLinkedList = new DoublyLinkedList<>();
        assertNull(doublyLinkedList.get(1));

        doublyLinkedList.add(1);
        doublyLinkedList.add(2);
        doublyLinkedList.add(3);

        assertEquals(1, doublyLinkedList.get(0));
        assertEquals(2, doublyLinkedList.get(1));
        assertEquals(3, doublyLinkedList.get(2));

        assertNull(doublyLinkedList.get(10));
    }

    @Test
    public void selectionSortTest(){
        doublyLinkedList.add(2);
        doublyLinkedList.add(5);
        doublyLinkedList.add(3);
        doublyLinkedList.add(1);
        doublyLinkedList.add(4);

        Integer[] array = new Integer[]{2,5,3,1,4};
        assertArrayEquals(array, doublyLinkedList.toArray());

        array = new Integer[]{1, 2, 3, 4, 5};
        doublyLinkedList.selectionSort();
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(5) head: null <- prev, data = 1, sig -> {1 <- prev, data = 2, sig -> {2 <- prev, data = 3, sig -> {3 <- prev, data = 4, sig -> {4 <- prev, data = 5, sig -> null}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add(-5);
        assertEquals(1, doublyLinkedList.get(0));
        array = new Integer[]{-5, 1, 2, 3, 4, 5};
        doublyLinkedList.selectionSort();
        assertArrayEquals(array, doublyLinkedList.toArray());
        assertEquals(-5, doublyLinkedList.get(0));

        array = new Integer[]{-5, 1, 2, 3, 4, 5, 1000};
        doublyLinkedList.add(1000);
        doublyLinkedList.selectionSort();
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(-5, doublyLinkedList.get(0));
        assertEquals(1, doublyLinkedList.get(1));
        assertEquals(2, doublyLinkedList.get(2));
        assertEquals(3, doublyLinkedList.get(3));
        assertEquals(4, doublyLinkedList.get(4));
        assertEquals(5, doublyLinkedList.get(5));
        assertEquals(1000, doublyLinkedList.get(6));

        assertEquals(
                "(7) head: null <- prev, data = -5, sig -> {-5 <- prev, data = 1, sig -> {1 <- prev, data = 2, sig -> {2 <- prev, data = 3, sig -> {3 <- prev, data = 4, sig -> {4 <- prev, data = 5, sig -> {5 <- prev, data = 1000, sig -> null}}}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add(10);
        assertEquals(10, doublyLinkedList.get(7));
        doublyLinkedList.selectionSort();
        assertEquals(10, doublyLinkedList.get(6));
        assertEquals(1000, doublyLinkedList.get(7));

        doublyLinkedList.add(1);
        assertEquals(1, doublyLinkedList.get(8));
        doublyLinkedList.selectionSort();
        assertEquals(1000, doublyLinkedList.get(8));
        assertEquals(1, doublyLinkedList.get(1));
        assertEquals(1, doublyLinkedList.get(2));
    }

    @Test
    public void bubbleSortTest(){
        doublyLinkedList.add(7);
        doublyLinkedList.add(0);
        doublyLinkedList.add(3);
        doublyLinkedList.add(6);
        doublyLinkedList.add(10);

        Integer[] array = new Integer[]{7,0,3,6,10};
        assertArrayEquals(array, doublyLinkedList.toArray());

        array = new Integer[]{0,3,6,7,10};
        doublyLinkedList.bubbleSort();
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(5) head: null <- prev, data = 0, sig -> {0 <- prev, data = 3, sig -> {3 <- prev, data = 6, sig -> {6 <- prev, data = 7, sig -> {7 <- prev, data = 10, sig -> null}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add(-1);
        assertEquals(0, doublyLinkedList.get(0));
        array = new Integer[]{-1, 0, 3, 6, 7, 10};
        doublyLinkedList.bubbleSort();
        assertArrayEquals(array, doublyLinkedList.toArray());
        assertEquals(-1, doublyLinkedList.get(0));

        doublyLinkedList.add(5);
        array = new Integer[]{-1, 0, 3, 5, 6, 7, 10};
        doublyLinkedList.bubbleSort();
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(-1, doublyLinkedList.get(0));
        assertEquals(0, doublyLinkedList.get(1));
        assertEquals(3, doublyLinkedList.get(2));
        assertEquals(5, doublyLinkedList.get(3));
        assertEquals(6, doublyLinkedList.get(4));
        assertEquals(7, doublyLinkedList.get(5));
        assertEquals(10, doublyLinkedList.get(6));

        doublyLinkedList.add(8);
        assertEquals(8, doublyLinkedList.get(7));
        assertEquals(10, doublyLinkedList.get(6));
        doublyLinkedList.bubbleSort();
        assertEquals(8, doublyLinkedList.get(6));
        assertEquals(10, doublyLinkedList.get(7));

        doublyLinkedList.add(3);
        assertEquals(3, doublyLinkedList.get(8));
        doublyLinkedList.bubbleSort();
        assertEquals(10, doublyLinkedList.get(8));
        assertEquals(3, doublyLinkedList.get(2));
        assertEquals(3, doublyLinkedList.get(3));
    }

    @Test
    public void reverseTest() {
        doublyLinkedList.add(1);
        doublyLinkedList.add(2);
        doublyLinkedList.add(3);
        doublyLinkedList.add(4);
        doublyLinkedList.add(5);

        Integer[] array = new Integer[]{1,2,3,4,5};
        assertArrayEquals(array, doublyLinkedList.toArray());

        doublyLinkedList.reverse();

        array = new Integer[]{5,4,3,2,1};
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(5) head: null <- prev, data = 5, sig -> {5 <- prev, data = 4, sig -> {4 <- prev, data = 3, sig -> {3 <- prev, data = 2, sig -> {2 <- prev, data = 1, sig -> null}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.reverse();
        array = new Integer[]{1,2,3,4,5};
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(5) head: null <- prev, data = 1, sig -> {1 <- prev, data = 2, sig -> {2 <- prev, data = 3, sig -> {3 <- prev, data = 4, sig -> {4 <- prev, data = 5, sig -> null}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.add(6);
        doublyLinkedList.add(7);
        array = new Integer[]{1,2,3,4,5,6,7};
        assertArrayEquals(array, doublyLinkedList.toArray());

        doublyLinkedList.reverse();
        array = new Integer[]{7,6,5,4,3,2,1};
        assertArrayEquals(array, doublyLinkedList.toArray());

        assertEquals(
                "(7) head: null <- prev, data = 7, sig -> {7 <- prev, data = 6, sig -> {6 <- prev, data = 5, sig -> {5 <- prev, data = 4, sig -> {4 <- prev, data = 3, sig -> {3 <- prev, data = 2, sig -> {2 <- prev, data = 1, sig -> null}}}}}}}",
                doublyLinkedList.toString()
        );

        doublyLinkedList.reverse();
        doublyLinkedList.reverse();
        assertArrayEquals(array, doublyLinkedList.toArray());
    }
}
