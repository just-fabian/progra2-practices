package tarea08LinkedList;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NodeTest {
    @Test
    public void test2() {
        Node n = new Node();
        n.data = 1337;
        n.next = new Node();
        n.next.data = 42;
        n.next.next = new Node();
        n.next.next.data = 23;
        try{
            assertEquals(Node.getNth(n, 0), 1337);
            assertEquals(Node.getNth(n, 1), 42);
            assertEquals(Node.getNth(n, 2), 23);
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void testNull() {
        try{
            Node.getNth(null, 0);
            assertTrue(false);
        }catch(Exception e){
            assertTrue(true);
        }
    }

    @Test
    public void testWrongIdx() {
        try{
            Node.getNth(new Node(), 1);
            assertTrue(false);
        }catch(Exception e){
            assertTrue(true);
        }
    }

    @Test
    public void twoEmpty() throws Exception {
        assertNull( Node.append( null, null ) );
    }

    @Test
    public void oneEmpty() throws Exception {
        NodeHelper.assertEqual( Node.append( null, new Node( 1 ) ), new Node( 1 ) );
        NodeHelper.assertEqual( Node.append( new Node( 1 ), null ), new Node( 1 ) );
    }

    @Test
    public void oneOne() throws Exception {
        NodeHelper.assertEqual( Node.append( new Node( 1 ), new Node( 2 ) ), NodeHelper.build( new int[] { 1, 2 } ) );
        NodeHelper.assertEqual( Node.append( new Node( 2 ), new Node( 1 ) ), NodeHelper.build( new int[] { 2, 1 } ) );
    }

    @Test
    public void bigLists() throws Exception {
        NodeHelper.assertEqual(
                Node.append( NodeHelper.build( new int[] { 1, 2 } ), NodeHelper.build( new int[] { 3, 4 } ) ),
                NodeHelper.build( new int[] { 1, 2, 3, 4 } )
        );
        NodeHelper.assertEqual(
                Node.append( NodeHelper.build( new int[] { 1, 2, 3, 4, 5 } ), NodeHelper.build( new int[] { 6, 7, 8 } ) ),
                NodeHelper.build( new int[] { 1, 2, 3, 4, 5, 6, 7, 8 } )
        );
    }
}
