package tarea10.bags;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestLinkedBagSorts {
    Bag<Integer> bag;
    @BeforeEach
    public void init(){
        bag = new LinkedBag<>();
        bag.add(3);
        bag.add(7);
        bag.add(5);
    }

    @AfterEach
    public void printSortedBag(){
        System.out.println(bag);
    }

    @Test
    public void testSelectionSort(){
        assertEquals("(3)root={data=5, sig->{data=7, sig->{data=3, sig->null}}}", bag.toString());
        bag.selectionSort();
        assertEquals("(3)root={data=3, sig->{data=5, sig->{data=7, sig->null}}}", bag.toString());
        bag.add(3);
        bag.add(10);
        assertEquals("(5)root={data=10, sig->{data=3, sig->{data=3, sig->{data=5, sig->{data=7, sig->null}}}}}", bag.toString());
        bag.selectionSort();
        assertEquals("(5)root={data=3, sig->{data=3, sig->{data=5, sig->{data=7, sig->{data=10, sig->null}}}}}", bag.toString());
        bag.add(11);
        assertEquals("(6)root={data=11, sig->{data=3, sig->{data=3, sig->{data=5, sig->{data=7, sig->{data=10, sig->null}}}}}}", bag.toString());
        bag.selectionSort();
        assertEquals("(6)root={data=3, sig->{data=3, sig->{data=5, sig->{data=7, sig->{data=10, sig->{data=11, sig->null}}}}}}", bag.toString());
    }
    @Test
    public void testBubbleSort(){
        assertEquals("(3)root={data=5, sig->{data=7, sig->{data=3, sig->null}}}", bag.toString());
        bag.bubbleSort();
        assertEquals("(3)root={data=3, sig->{data=5, sig->{data=7, sig->null}}}", bag.toString());
        bag.add(3);
        bag.add(10);
        assertEquals("(5)root={data=10, sig->{data=3, sig->{data=3, sig->{data=5, sig->{data=7, sig->null}}}}}", bag.toString());
        bag.bubbleSort();
        assertEquals("(5)root={data=3, sig->{data=3, sig->{data=5, sig->{data=7, sig->{data=10, sig->null}}}}}", bag.toString());
        bag.add(11);
        assertEquals("(6)root={data=11, sig->{data=3, sig->{data=3, sig->{data=5, sig->{data=7, sig->{data=10, sig->null}}}}}}", bag.toString());
        bag.bubbleSort();
        assertEquals("(6)root={data=3, sig->{data=3, sig->{data=5, sig->{data=7, sig->{data=10, sig->{data=11, sig->null}}}}}}", bag.toString());
    }
}

