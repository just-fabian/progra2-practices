package tarea03;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FrequentExampleTests {
    @Test
    public void tests() {
        assertEquals(2, new Kata().mostFrequentItemCount(new int[] {3, -1, -1}));
        assertEquals(5, new Kata().mostFrequentItemCount(new int[] {3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3}));
        assertEquals(8, new Kata().mostFrequentItemCount(new int[] {1,2,4,4,5,5,5,3,3,3,3,3,3,3,3}));
        assertEquals(2, new Kata().mostFrequentItemCount(new int[] {1,1,2}));
        assertEquals(3, new Kata().mostFrequentItemCount(new int[] {1,1,2,1}));
    }
}
