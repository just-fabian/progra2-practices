package tarea06;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {
    static Game game;
    static Player player1;
    static int lastPosPlayerX, lastPosPlayerY;

    @BeforeEach
    public void reset(){
        game = new Game(1, 5);
        player1 = new Player("player");
        game.addPlayer(player1);
        lastPosPlayerX = player1.getPositionX();
        lastPosPlayerY = player1.getPositionY();
    }

    @Test
    public void obstaclesNumber(){
        assertEquals(5, game.obstacles.length);
    }

    @Test
    public void playersNumber(){
        assertEquals(1, game.players.length);
    }

    @Test
    public void movePlayerRight(){
        System.out.println("BEFORE MOVING RIGHT\n" + game.printBoard());

        if (player1.getPositionX() != 9 && game.posibleMove(player1.getPositionY(), player1.getPositionX() + 1)) {
            game.movePlayerRight(player1);
            System.out.println("AFTER MOVING RIGHT\n" + game.printBoard());
            assertEquals(lastPosPlayerY, player1.getPositionY());
            assertEquals(lastPosPlayerX + 1, player1.getPositionX());
        } else System.out.println("Not possible to move to right");
    }

    @Test
    public void movePlayerLeft(){
        System.out.println("BEFORE MOVING LEFT\n" + game.printBoard());

        if (player1.getPositionX() != 0 && game.posibleMove(player1.getPositionY(), player1.getPositionX() - 1)) {
            game.movePlayerLeft(player1);
            System.out.println("AFTER MOVING LEFT\n" + game.printBoard());
            assertEquals(lastPosPlayerY, player1.getPositionY());
            assertEquals(lastPosPlayerX - 1, player1.getPositionX());
        } else System.out.println("Not possible to move to left");
    }

    @Test
    public void movePlayerDown(){
        System.out.println("BEFORE MOVING DOWN\n" + game.printBoard());

        if (player1.getPositionY() != 9 && game.posibleMove(player1.getPositionY() + 1, player1.getPositionX())){
            game.movePlayerDown(player1);
            System.out.println("AFTER MOVING DOWN\n" + game.printBoard());
            assertEquals(lastPosPlayerY + 1, player1.getPositionY());
            assertEquals(lastPosPlayerX, player1.getPositionX());
        } else System.out.println("Not possible to move to down");
    }

    @Test
    public void movePlayerUp(){
        System.out.println("BEFORE MOVING UP\n" + game.printBoard());
        if(player1.getPositionY() != 0 && game.posibleMove(player1.getPositionY() - 1, player1.getPositionX())){
            game.movePlayerUp(player1);
            System.out.println("AFTER MOVING UP\n" + game.printBoard());
            assertEquals(lastPosPlayerY - 1, player1.getPositionY());
            assertEquals(lastPosPlayerX, player1.getPositionX());
        } else System.out.println("Not possible to move to up");
    }

    @Test
    public void notPossibleMove(){
        game.board[8][7] = new Obstacle(8, 7);
        assertFalse(game.posibleMove(8, 7));
    }

    @Test
    public void possibleMove(){
        game.board[8][7] = new EmptySpace(8, 7);
        assertTrue(game.posibleMove(8, 7));
    }
}
