package tarea07;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

public class JUArrayListTest {
    JUArrayList<Integer> numeros;

    @BeforeEach
    public void init(){
        numeros = new JUArrayList<>();
    }

    @Test
    public void addSizeGetTest(){
        numeros.add(1);
        numeros.add(2);
        System.out.println(numeros);

        assertEquals(2, numeros.size());
        assertEquals(1, numeros.get(0));
        assertEquals(2, numeros.get(1));
    }

    @Test
    public void isEmptyTest(){
        assertTrue(numeros.isEmpty());

        numeros.add(1);
        numeros.add(2);
        System.out.println(numeros);
        assertFalse(numeros.isEmpty());

        numeros.clear();
        System.out.println(numeros);
        assertTrue(numeros.isEmpty());
    }

    @Test
    public void iteratorTest(){
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);

        int iteratorCounter = 0;
        Iterator<Integer> iterator = numeros.iterator();
        while(iterator.hasNext()) {
            assertEquals(numeros.get(iteratorCounter), iterator.next());
            iteratorCounter++;
        }
        assertEquals(3, iteratorCounter);
    }

    @Test
    public void addByIndexTest(){
        numeros.add(1);
        numeros.add(2);
        numeros.add(7);
        numeros.add(1, 5);

        System.out.println(numeros);
        assertEquals(4, numeros.size());
        assertEquals(1, numeros.get(0));
        assertEquals(5, numeros.get(1));
        assertEquals(2, numeros.get(2));
        assertEquals(7, numeros.get(3));
    }

    @Test
    public void removeTest(){
        numeros.add(1);
        numeros.add(5);
        numeros.remove(numeros.get(0));

        System.out.println(numeros);

        assertEquals(1, numeros.size());
        assertEquals(5, numeros.get(0));
    }

    @Test
    public void removeByIndexTest(){
        numeros.add(2);
        numeros.add(5);
        numeros.add(7);
        Integer removed = numeros.remove(1);

        System.out.println(numeros);

        assertEquals(5, removed);
        assertEquals(2, numeros.size());
        assertEquals(7, numeros.get(1));
    }

    @Test
    public void clearTest(){
        numeros.add(2);
        numeros.add(5);
        numeros.add(7);
        System.out.println(numeros);

        numeros.clear();
        System.out.println(numeros);
        assertEquals(0, numeros.size());

        numeros.add(1);
        numeros.add(2);
        System.out.println(numeros);
        assertEquals(2, numeros.size());
        assertEquals(1, numeros.get(0));
        assertEquals(2, numeros.get(1));
    }

    @Test
    public void containsTest(){
        numeros.add(1);
        numeros.add(2);

        Integer integerIn = 1;
        Integer integerNot = 5;

        assertTrue(numeros.contains(integerIn));
        assertFalse(numeros.contains(integerNot));
    }

    @Test
    public void setTest(){
        numeros.add(9);
        numeros.add(10);
        numeros.add(11);

        numeros.set(1, 12);

        assertEquals(12, numeros.get(1));
    }

    @Test
    public void toArrayTest(){
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);

        Integer[] array = new Integer[]{1,2,3};
        for (int i = 0; i < array.length; i++){
            assertEquals(array[i], numeros.toArray()[i]);
        }
    }

    @Test
    public void indexOfTest(){
        numeros.add(1);
        numeros.add(3);
        numeros.add(3);
        numeros.add(4);
        numeros.add(2, 6);

        assertEquals(1, numeros.indexOf(3));
        assertEquals(2, numeros.indexOf(6));
        assertEquals(4, numeros.indexOf(4));
    }

    @Test
    public void lastIndexOfTest(){
        numeros.add(1);
        numeros.add(3);
        numeros.add(3);
        numeros.add(4);
        numeros.add(3);

        assertEquals(4, numeros.lastIndexOf(3));
    }
}
