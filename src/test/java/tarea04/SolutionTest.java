package tarea04;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SolutionTest {
    @Test
    public void basicTests() {
        assertEquals(6, Solution.sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9}));
        assertEquals(5, Solution.sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 3, 3, 3, 9, 2}));
        assertEquals(1, Solution.sumGroups(new int[] {2}));
        assertEquals(2, Solution.sumGroups(new int[] {1,2}));
        assertEquals(1, Solution.sumGroups(new int[] {1,1,2,2}));
        assertEquals(65, Solution.sumGroups(new int[] {321, 909, 871, 908, 852, 739, 762, 494, 511, 282, 286, 456, 48, 291, 917, 278, 420, 866, 149, 378, 559, 63, 654, 821, 186, 829, 879, 562, 944, 241, 846, 708, 909, 702, 317, 715, 455, 606, 726, 609, 610, 132, 768, 685, 120, 416, 68, 535, 595, 999, 885, 205, 144, 539, 857, 107, 660, 161, 98, 302, 743, 701, 252, 10, 183, 342, 836, 208, 986, 739, 190, 25, 164, 55, 104, 46, 543, 960, 226, 930, 626, 109, 922, 782, 145, 87, 903, 245, 179, 610, 425, 703, 746, 594, 964, 751, 387, 573, 254, 956, 949, 738, 889, 938, 90, 972, 979, 208, 970, 427, 411, 525, 888, 120, 718, 624, 462, 441, 334, 283, 506, 625, 212, 604, 550, 397, 689, 164, 726, 273, 78, 368, 717, 383, 149, 85, 516, 479, 467, 863, 75, 167, 198, 414, 618, 904, 747, 128, 738, 364, 17, 243, 848, 450, 331, 350, 246, 896, 70, 785}));
    }

}
