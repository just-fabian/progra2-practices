package tarea01.mysteryColors;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MysteryColorsAnalyzerImplTest {
    private MysteryColorAnalyzerImpl mysteryColorAnalyzerImpl;
    private List<Color> colors;

    @Test
    public void testNumberOfDistinctColors(){
        mysteryColorAnalyzerImpl = new MysteryColorAnalyzerImpl();
        colors = new ArrayList<>(List.of(Color.RED, Color.GREEN, Color.BLUE, Color.RED));
        assertEquals(3, mysteryColorAnalyzerImpl.numberOfDistinctColors(colors));
    }

    @Test
    public void testOneColorOccurrence(){
        mysteryColorAnalyzerImpl = new MysteryColorAnalyzerImpl();
        colors = new ArrayList<>(List.of(Color.RED, Color.GREEN, Color.BLUE, Color.RED));
        assertEquals(1, mysteryColorAnalyzerImpl.colorOccurrence(colors, Color.BLUE));
    }

    @Test
    public void testThreeColorOccurrence(){
        mysteryColorAnalyzerImpl = new MysteryColorAnalyzerImpl();
        colors = new ArrayList<>(List.of(Color.RED, Color.GREEN, Color.BLUE, Color.RED, Color.RED));
        assertEquals(3, mysteryColorAnalyzerImpl.colorOccurrence(colors, Color.RED));
    }
}
