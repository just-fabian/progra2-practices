package tarea13;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StackStringTest {
    Stack<String> stack;

    @BeforeEach
    public void init(){
        stack = new Stack<>();
    }

    @Test
    public void pushTest(){
        assertTrue(stack.isEmpty());
        stack.push("Rojo");
        assertEquals("Rojo", stack.get(0));
        assertFalse(stack.isEmpty());

        assertEquals(1, stack.size());
        stack.push("Naranja");
        stack.push("Azul");
        assertEquals("Azul", stack.get(0));
        assertEquals(3, stack.size());

        stack.push("Amarillo");
        stack.push("Verde");
        stack.push("Negro");
        assertFalse(stack.isEmpty());
        assertEquals("Negro", stack.get(0));
        assertEquals(6, stack.size());
    }

    @Test
    public void popTest(){
        stack.push("Argentina");
        stack.push("Bolivia");
        stack.push("Brasil");
        assertEquals("Brasil", stack.get(0));
        assertFalse(stack.isEmpty());
        assertEquals(3, stack.size());

        assertEquals("Brasil", stack.pop());
        assertEquals("Bolivia", stack.get(0));
        assertEquals(2, stack.size());

        assertEquals("Bolivia", stack.pop());
        assertEquals("Argentina", stack.get(0));
        assertEquals(1, stack.size());

        assertFalse(stack.isEmpty());
        assertEquals("Argentina", stack.pop());
        assertEquals(0, stack.size());
        assertTrue(stack.isEmpty());

        assertNull(stack.pop());

        stack.push("Colombia");
        assertEquals("Colombia", stack.get(0));

        stack.push("Paraguay");
        stack.push("Uruguay");
        assertEquals("Uruguay", stack.get(0));
        assertFalse(stack.isEmpty());
        assertEquals(3, stack.size());
        assertEquals("Uruguay", stack.pop());
        assertEquals("Paraguay", stack.get(0));

        assertEquals("Paraguay", stack.pop());
        assertEquals(1, stack.size());
        assertFalse(stack.isEmpty());

        assertEquals("Colombia", stack.pop());
        assertEquals(0, stack.size());
        assertTrue(stack.isEmpty());
    }
}
