package tarea13;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StackIntegerTest {

    Stack<Integer> stack;

    @BeforeEach
    public void init(){
        stack = new Stack<>();
    }

    @Test
    public void pushTest(){
        assertTrue(stack.isEmpty());
        stack.push(1);
        assertEquals(1, stack.get(0));
        assertFalse(stack.isEmpty());

        assertEquals(1, stack.size());
        stack.push(2);
        stack.push(3);
        assertEquals(3, stack.get(0));
        assertEquals(3, stack.size());

        stack.push(4);
        stack.push(5);
        stack.push(6);
        assertFalse(stack.isEmpty());
        assertEquals(6, stack.get(0));
        assertEquals(6, stack.size());
    }

    @Test
    public void popTest(){
        stack.push(1);
        stack.push(5);
        stack.push(2);
        assertEquals(2, stack.get(0));
        assertFalse(stack.isEmpty());
        assertEquals(3, stack.size());

        assertEquals(2, stack.pop());
        assertEquals(5, stack.get(0));
        assertEquals(2, stack.size());

        assertEquals(5, stack.pop());
        assertEquals(1, stack.get(0));
        assertEquals(1, stack.size());

        assertFalse(stack.isEmpty());
        assertEquals(1, stack.pop());
        assertEquals(0, stack.size());
        assertTrue(stack.isEmpty());

        assertNull(stack.pop());

        stack.push(2);
        assertEquals(2, stack.get(0));

        stack.push(8);
        assertEquals(8, stack.get(0));
        assertFalse(stack.isEmpty());
        assertEquals(2, stack.size());
        assertEquals(8, stack.pop());
        assertEquals(2, stack.get(0));

        assertEquals(2, stack.pop());
        assertEquals(0, stack.size());
        assertTrue(stack.isEmpty());
    }
}

