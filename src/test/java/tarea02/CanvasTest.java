package tarea02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CanvasTest {
    @Test
    public void drawLines() {
        Canvas c = new Canvas(5, 5);
        c.draw(0, 2, 4, 2).draw(2, 0, 2, 4);

        String solution = "-------\n|  x  |\n|  x  |\n|xxxxx|\n|  x  |\n|  x  |\n-------";
        System.out.println(solution);

        String answer = c.drawCanvas();
        System.out.println(answer);

        assertEquals(solution, answer);
    }

    @Test
    public void drawRectangle() {
        Canvas c = new Canvas(7, 7);
        c.draw(1, 1, 5, 4);

        String solution = "---------\n|       |\n| xxxxx |\n| x   x |\n| x   x |\n| xxxxx |\n|       |\n|       |\n---------";

        String answer = c.drawCanvas();
        System.out.println(answer);

        assertEquals(solution, answer);
    }

//    @Test
//    public void fill() {
//        Canvas c = new Canvas(7, 7);
//        c.draw(1, 1, 5, 4).fill(3, 3, 'o');
//        System.out.println("---------\n|       |\n| xxxxx |\n| xooox |\n| xooox |\n| xxxxx |\n|       |\n|       |\n---------");
//        assertEquals("---------\n|       |\n| xxxxx |\n| xooox |\n| xooox |\n| xxxxx |\n|       |\n|       |\n---------", c.drawCanvas());
//    }
}