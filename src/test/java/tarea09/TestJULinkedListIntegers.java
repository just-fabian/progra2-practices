package tarea09;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestJULinkedListIntegers {
    JULinkedList<Integer> linkedList;

    @BeforeEach
    public void init(){
        linkedList = new JULinkedList<>();
    }

    @Test
    public void testAddGetSize(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        System.out.println(linkedList);

        assertEquals(4, linkedList.size());
        assertEquals(1, linkedList.get(0));
        assertEquals(2, linkedList.get(1));
        assertEquals(3, linkedList.get(2));
        assertEquals(4, linkedList.get(3));
    }

    @Test
    public void clearTest(){
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        System.out.println(linkedList);

        linkedList.clear();
        System.out.println(linkedList);
        assertEquals(0, linkedList.size());

        linkedList.add(1);
        linkedList.add(2);
        System.out.println(linkedList);
        assertEquals(2, linkedList.size());
        assertEquals(1, linkedList.get(0));
        assertEquals(2, linkedList.get(1));
    }

    @Test
    public void isEmptyTest(){
        assertTrue(linkedList.isEmpty());

        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        System.out.println(linkedList);
        assertFalse(linkedList.isEmpty());

        linkedList.clear();
        System.out.println(linkedList);
        assertTrue(linkedList.isEmpty());
    }

    @Test
    public void iteratorTest(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(5);

        int iteratorCounter = 0;
        Iterator<Integer> iterator = linkedList.iterator();
        while(iterator.hasNext()) {
            assertEquals(linkedList.get(iteratorCounter), iterator.next());
            iteratorCounter++;
        }
        assertEquals(5, iteratorCounter);

        assertFalse(iterator.hasNext());
        linkedList.add(6);
        assertTrue(iterator.hasNext());

        linkedList.clear();
        iterator = linkedList.iterator();
        assertFalse(iterator.hasNext());
    }

    @Test
    public void removeTest(){
        linkedList.add(1);
        linkedList.add(2);

        Integer integerToRemove = 1;
        Integer integerNotInList = 5;

        assertTrue(linkedList.remove(integerToRemove));
        assertFalse(linkedList.remove(integerNotInList));

        System.out.println(linkedList);

        assertEquals(1, linkedList.size());
        assertEquals(2, linkedList.get(0));
    }

    @Test
    public void containsTest(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Integer integerIn = 1;
        Integer integerNot = 10;

        assertTrue(linkedList.contains(integerIn));
        assertFalse(linkedList.contains(integerNot));
    }

    @Test
    public void removeByIndexTest(){
        linkedList.add(2);
        linkedList.add(5);
        linkedList.add(7);
        Integer removed = linkedList.remove(1);

        System.out.println(linkedList);

        assertEquals(5, removed);
        assertEquals(2, linkedList.size());
        assertEquals(7, linkedList.get(1));

        // Not valid index:
        assertNull(linkedList.remove(-1));
        assertNull(linkedList.remove(4));
    }

    @Test
    public void setTest(){
        linkedList.add(9);
        linkedList.add(10);
        linkedList.add(11);

        assertEquals(10, linkedList.set(1, 12));
        assertEquals(12, linkedList.get(1));

        // Not valid index:
        assertNull(linkedList.set(3, 12));
        assertNull(linkedList.set(-1, 12));
    }

    @Test
    public void addByIndexTest(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(7);
        System.out.println(linkedList);


        linkedList.add(1, 5);
        assertEquals(5, linkedList.get(1));
        assertEquals(2, linkedList.get(2));

        linkedList.add(0, 10);
        System.out.println(linkedList);

        assertEquals(10, linkedList.get(0));
        assertEquals(1, linkedList.get(1));
        assertEquals(5, linkedList.get(2));
        assertEquals(2, linkedList.get(3));
        assertEquals(7, linkedList.get(4));

        assertEquals(5, linkedList.size());

        linkedList.clear();
        linkedList.add(0, 0);
        assertEquals(0, linkedList.get(0));
    }

    @Test
    public void toArrayTest(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);

        Integer[] array = new Integer[]{1,2,3,4};
        for (int i = 0; i < array.length; i++){
            assertEquals(array[i], linkedList.toArray()[i]);
        }

        linkedList.remove(2);
        linkedList.add(2, 6);

        array = new Integer[]{1,2,6,4};
        for (int i = 0; i < array.length; i++){
            assertEquals(array[i], linkedList.toArray()[i]);
        }
    }

    @Test
    public void subListTest(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(5);
        linkedList.add(6);

        Integer[] array = new Integer[]{2,3,4};
        for (int i = 0; i < array.length; i++){
            assertEquals(array[i], linkedList.subList(1,4).get(i));
        }

        assertEquals(0, linkedList.subList(2,2).size());
    }

    @Test
    public void indexOfTest(){
        linkedList.add(1);
        linkedList.add(3);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(2, 6);

        assertEquals(1, linkedList.indexOf(3));
        assertEquals(2, linkedList.indexOf(6));
        assertEquals(4, linkedList.indexOf(4));
    }

    @Test
    public void lastIndexOfTest(){
        linkedList.add(1);
        linkedList.add(3);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(3);

        assertEquals(4, linkedList.lastIndexOf(3));
        assertEquals(0, linkedList.lastIndexOf(1));
    }
}
