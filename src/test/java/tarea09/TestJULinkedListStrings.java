package tarea09;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestJULinkedListStrings {
    JULinkedList<String> linkedList;

    @BeforeEach
    public void init(){
        linkedList = new JULinkedList<>();
    }

    @Test
    public void testAddGetSize(){
        linkedList.add("Rojo");
        linkedList.add("Negro");
        linkedList.add("Naranja");
        linkedList.add("Verde");
        System.out.println(linkedList);

        assertEquals(4, linkedList.size());
        assertEquals("Rojo", linkedList.get(0));
        assertEquals("Negro", linkedList.get(1));
        assertEquals("Naranja", linkedList.get(2));
        assertEquals("Verde", linkedList.get(3));

        linkedList.add("Azul");
        linkedList.add("Blanco");
        assertEquals(6, linkedList.size());
        assertEquals("Azul", linkedList.get(4));
        assertEquals("Blanco", linkedList.get(5));
    }

    @Test
    public void clearTest(){
        linkedList.add("manzana");
        linkedList.add("mango");
        linkedList.add("mandarina");
        System.out.println(linkedList);
        assertEquals(3, linkedList.size());

        linkedList.clear();
        System.out.println(linkedList);
        assertEquals(0, linkedList.size());

        linkedList.add("melon");
        linkedList.add("mora");
        System.out.println(linkedList);
        assertEquals(2, linkedList.size());
        assertEquals("melon", linkedList.get(0));
        assertEquals("mora", linkedList.get(1));
    }

    @Test
    public void isEmptyTest(){
        assertTrue(linkedList.isEmpty());

        linkedList.add("f");
        linkedList.add("g");
        linkedList.add("h");
        System.out.println(linkedList);
        assertFalse(linkedList.isEmpty());

        linkedList.clear();
        System.out.println(linkedList);
        assertTrue(linkedList.isEmpty());
    }

    @Test
    public void iteratorTest(){
        linkedList.add("chelsea");
        linkedList.add("real madrid");
        linkedList.add("manchester city");
        linkedList.add("bayern munich");

        int iteratorCounter = 0;
        Iterator<String> iterator = linkedList.iterator();
        while(iterator.hasNext()) {
            assertEquals(linkedList.get(iteratorCounter), iterator.next());
            iteratorCounter++;
        }
        assertEquals(4, iteratorCounter);

        assertFalse(iterator.hasNext());
        linkedList.add("liverpool");
        assertTrue(iterator.hasNext());

        linkedList.clear();
        iterator = linkedList.iterator();
        assertFalse(iterator.hasNext());
    }

    @Test
    public void removeTest(){
        linkedList.add("xyz");
        linkedList.add("abc");

        String removed = "xyz";
        String notInList = "def";

        assertTrue(linkedList.remove(removed));
        assertFalse(linkedList.remove(notInList));

        assertEquals(1, linkedList.size());
        assertEquals("abc", linkedList.get(0));
    }

    @Test
    public void containsTest(){
        linkedList.add("jeff");
        linkedList.add("er");
        linkedList.add("son");

        String stringIn = "son";
        String stringNotIn = "jefferson";

        assertTrue(linkedList.contains(stringIn));
        assertFalse(linkedList.contains(stringNotIn));
    }

    @Test
    public void removeByIndexTest(){
        linkedList.add("nicol");
        linkedList.add("adrian");
        linkedList.add("carlos");
        linkedList.add("camila");
        String removed = linkedList.remove(2);

        System.out.println(linkedList);

        assertEquals("carlos", removed);
        assertEquals(3, linkedList.size());
        assertEquals("camila", linkedList.get(2));

        // Not valid index:
        assertNull(linkedList.remove(-1));
        assertNull(linkedList.remove(4));
    }

    @Test
    public void setTest(){
        linkedList.add("salteña");
        linkedList.add("de");
        linkedList.add("chocolate");

        assertEquals("chocolate", linkedList.set(2, "pollo"));
        assertEquals("pollo", linkedList.get(2));

        // Not valid index:
        assertNull(linkedList.set(3, "carne"));
        assertNull(linkedList.set(-1, "picante"));
    }

    @Test
    public void addByIndexTest(){
        linkedList.add("batman");
        linkedList.add("flash");
        linkedList.add("wonder woman");
        System.out.println(linkedList);

        linkedList.add(1, "superman");
        assertEquals("superman", linkedList.get(1));
        assertEquals("flash", linkedList.get(2));

        linkedList.add(0, "aquaman");
        System.out.println(linkedList);

        assertEquals("aquaman", linkedList.get(0));
        assertEquals("batman", linkedList.get(1));
        assertEquals("superman", linkedList.get(2));
        assertEquals("flash", linkedList.get(3));
        assertEquals("wonder woman", linkedList.get(4));

        assertEquals(5, linkedList.size());

        linkedList.clear();
        linkedList.add(0, "catwoman");
        assertEquals("catwoman", linkedList.get(0));
    }

    @Test
    public void toArrayTest(){
        linkedList.add("xiaomi");
        linkedList.add("huawei");
        linkedList.add("samsung");
        linkedList.add("apple");
        linkedList.add("lg");
        linkedList.add("sony");

        String[] array = new String[]{"xiaomi", "huawei", "samsung", "apple", "lg", "sony"};
        for (int i = 0; i < array.length; i++){
            assertEquals(array[i], linkedList.toArray()[i]);
        }

        linkedList.remove(4);
        linkedList.add(4, "honor");
        System.out.println(linkedList);

        array = new String[]{"xiaomi", "huawei", "samsung", "apple", "honor", "sony"};
        for (int i = 0; i < array.length; i++){
            assertEquals(array[i], linkedList.toArray()[i]);
        }
    }

    @Test
    public void subListTest(){
        linkedList.add("Bolivia");
        linkedList.add("Argentina");
        linkedList.add("Chile");
        linkedList.add("Brasil");
        linkedList.add("Ecuador");
        linkedList.add("Colombia");

        String[] array = new String[]{"Bolivia", "Argentina", "Chile"};
        for (int i = 0; i < array.length; i++){
            assertEquals(array[i], linkedList.subList(0,3).get(i));
        }

        assertEquals(0, linkedList.subList(2,2).size());
    }

    @Test
    public void indexOfTest(){
        linkedList.add("silpancho");
        linkedList.add("pique macho");
        linkedList.add("falso conejo");
        linkedList.add("chanka de pollo");
        linkedList.add(2, "sopa de mani");

        assertEquals(1, linkedList.indexOf("pique macho"));
        assertEquals(0, linkedList.indexOf("silpancho"));
        assertEquals(2, linkedList.indexOf("sopa de mani"));
        assertEquals(3, linkedList.indexOf("falso conejo"));
        assertEquals(4, linkedList.indexOf("chanka de pollo"));
    }

    @Test
    public void lastIndexOfTest(){
        linkedList.add("tokyo");
        linkedList.add("rio");
        linkedList.add("el profesor");
        linkedList.add("nairobi");
        linkedList.add("el profesor");

        assertEquals(4, linkedList.lastIndexOf("el profesor"));
        assertEquals(1, linkedList.lastIndexOf("rio"));
    }
}
